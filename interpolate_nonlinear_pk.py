#!/usr/bin/python
"""Interpolate non-linear P(k, z) from CLASS."""

import numpy as np
import pylab as P
import scipy.interpolate
import os
import cosmolopy as cp

# Define default cosmology
DEFAULT_COSMO = {
 'omega_M_0' : 0.266,
 'omega_lambda_0' : 0.734,
 'omega_k_0' : 0.0,
 'omega_b_0' : 0.045,
 'omega_n_0' : 0.0,
 'omega_r_0' : 0.0001, # FIXME: radiation density
 'N_nu' : 0,
 'Y_He' : 0.24, # Helium fraction
 'h' : 0.71,
 'n' : 0.96,
 'sigma_8' : 0.8,
 'baryonic_effects': False,
 'gamma': 0.55,
 'tau0': 0., # Optical depth due to baryons today
 'z_re': 10. # Redshift of reionisation (instantaneous)
}

CLASS_DIR = "/home/phil/postgrad/software/class/output/"

# Load generated non-linear P(k) from files
zsamp = []
pk = []
kk = []
for item in os.listdir(CLASS_DIR):
  if "pk_nl.dat" in item:
    
    # Get redshift
    f = open(CLASS_DIR+item, 'r')
    zsamp.append(  float(f.readlines()[0].split("z=")[1])  )
    f.close()
    
    # Get P(k) data (all k should be the same)
    _k, _pk = np.genfromtxt(CLASS_DIR+item).T
    kk.append(_k)
    pk.append(_pk)
zsamp = np.array(zsamp)
kk = np.array(kk)
pk = np.array(pk)

# Sort results in redshift
idxs = np.argsort(zsamp)
zsamp = zsamp[idxs]
kk = kk[idxs] * 0.7
pk = pk[idxs] / (0.7)**3.

# Interpolation in 2D
K, Z = np.meshgrid(np.log10(kk[0]), zsamp)
coords = np.column_stack( (K.flatten(), Z.flatten()) )
PK = scipy.interpolate.LinearNDInterpolator(coords, np.log10(pk.flatten()))

# Linear P(k)
pklin = cp.perturbation.power_spectrum(kk[0], 0.0, **DEFAULT_COSMO)

# Plot results
P.subplot(111)
for i in range(zsamp.size):
  P.plot(kk[i], pk[i], color='0.5')
P.plot(kk[0], 10.**PK(np.log10(kk[0]), 1.3), 'r-', lw=1.4)
P.plot(kk[0], pklin, 'b-', lw=1.4)
P.xscale('log')
P.yscale('log')
P.show()

#!/usr/bin/python
"""
Demo for finding scalar products along straght lines in a spherical 
polar coordinate system.
"""

import numpy as np
import pylab as P

def los(ro, theta, phi, eta):
    """Draw a line from some start point, extending for some length."""
    # eta is a line in comoving space of some length
    # ro is the starting radius of some observer
    # theta, phi are the directions of the source on the observer's sky
    xo = ro; yo = 0.; zo = 0.
    
    # Position in global Cartesian coord. system
    _x = xo + eta*np.sin(theta)*np.cos(phi)
    _y = yo + eta*np.sin(theta)*np.sin(phi)
    _z = zo + eta*np.cos(theta)
    
    # Angle in global sph. polar coord. system
    th = np.arccos(_z/eta); th[np.isnan(th)] = np.pi/2.
    ph = np.arctan(_y/_x)
    ph[np.where(ph < 0.)] = np.pi + ph[np.where(ph < 0.)]
    r = np.sqrt(_x**2. + _y**2. + _z**2.)
    
    # Dot product
    dp =   np.sin(th)*np.cos(ph) * np.sin(theta)*np.cos(phi) \
         + np.sin(th)*np.sin(ph) * np.sin(theta)*np.sin(phi) \
         + np.cos(th) * np.cos(theta)
    return dp, th, ph, r, _x, _y, _z



eta = np.linspace(0., 100., 1000)
angles = np.linspace(0., np.pi, 11.)
#angles = np.linspace(0., 2.*np.pi, 11.)

P.subplot(111)
for a in angles:
    dp, th, ph, r, _x, _y, _z = los(20., np.pi/2., a, eta)
    #print "\t", round(a*180./np.pi, 1), "\n\t", dp, "\n\t", th, "\n\t", ph
    P.plot(eta, dp)
    #P.polar(ph, r)

#P.subplot(111)
#P.plot(r)
#P.plot(ph)


#P.plot(r, ph)
P.show()


exit()
angles = np.linspace(0., np.pi, 18.)
#dp = los(10., 0.5*np.pi, np.pi/4., eta)
dp = [los(10., 0., a, eta) for a in angles]

P.subplot(111)
for i in range(len(angles)):
    P.plot(eta, dp[i], label=str(round(angles[i]*180./np.pi, 1)) )
P.legend(loc='upper right', prop={'size':'x-small'})
P.show()

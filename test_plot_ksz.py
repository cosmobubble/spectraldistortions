#!/usr/bin/python
"""
Plot KSZ power spectrum at l=3000, using Zibin & Moss units;
T_0^2 l(l+1) C_l/2pi [uK^2]
"""

import numpy as np
import cosmolopy as cp
import pylab as P
import scipy.interpolate
from matplotlib import ticker
import distortions

cosmo = distortions.DEFAULT_COSMO
T0 = distortions.T0
l = 3000.

# Define parameter space
r0 = np.logspace(3., np.log10(1.2e4), 15)
phi0 = np.logspace(-4., -2., 25)

# Construct 2D plot from 1D values
R0, PHI0 = np.meshgrid(r0, phi0)
CL = np.load('test-cls.npy')

# Interpolate to find coords of some preferred value
val = 6.7 # uK, Reichardt et al 2012, Table 3, SPT 95% CL for most conservative(?) model.
phival = []
for i in range(r0.size):
  _phi0 = scipy.interpolate.interp1d(CL.T[i], phi0, kind='cubic')
  phival.append( _phi0(val) )
phival = np.array(phival)

# Plot linewidths
MP_LINEWIDTH = 2.4
MP_TICKSIZE = 10.

# Plot data
P.rc('axes', linewidth=MP_LINEWIDTH)
#P.rc('axes', markerlinesize=1.4)
P.subplot(111)
ctr = P.contour(R0, PHI0, CL, locator=ticker.LogLocator(), colors='k', linewidths=3.0)
P.clabel(ctr, inline=1, fontsize=20., fmt='%1.0f')
#P.clabel(ctr, inline=1, fontsize='small', fmt=ticker.LogFormatterMathtext())
P.plot(r0, phival, 'r-', lw=5.0)
P.xscale('log')
P.yscale('log')
P.xlabel("$r_0 \mathrm{[Mpc]}$", fontdict={'fontsize':'28'}, labelpad=-15.)
P.ylabel("$\Phi_0$", fontdict={'fontsize':'28'})

# Set font sizes
fontsize = 20.
for tick in P.gca().yaxis.get_major_ticks():
  tick.label1.set_fontsize(fontsize)
  tick.tick1line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick2line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick1line.set_markersize(MP_TICKSIZE)
  tick.tick2line.set_markersize(MP_TICKSIZE)
for tick in P.gca().yaxis.get_minor_ticks():
  tick.tick1line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick2line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick1line.set_markersize(0.5*MP_TICKSIZE)
  tick.tick2line.set_markersize(0.5*MP_TICKSIZE)
for tick in P.gca().xaxis.get_major_ticks():
  tick.label1.set_fontsize(fontsize)
  tick.tick1line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick2line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick1line.set_markersize(MP_TICKSIZE)
  tick.tick2line.set_markersize(MP_TICKSIZE)
for tick in P.gca().xaxis.get_minor_ticks():
  tick.tick1line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick2line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick1line.set_markersize(0.5*MP_TICKSIZE)
  tick.tick2line.set_markersize(0.5*MP_TICKSIZE)

P.show()

#!/usr/bin/python
"""
Handle likelihoods of various data we have.
"""

import numpy as np
import scipy.interpolate
import scipy.integrate
import scipy.optimize
import pylab as P


# SPT KSZ likelihood data at l=3000 (model dependent)
# From Fig. 9, left panel, red dashed line of arXiv:1111.0932v2
# FIXME: Probably not the one we want to use in the final analysis...
D_ksz, l_ksz = np.genfromtxt("spt_ksz_plot.dat").T
L_ksz = scipy.interpolate.interp1d(D_ksz, l_ksz, kind='linear', fill_value=0.)


# COBE y-parameter data, from Fixsen et al., arXiv:astro-ph/9605054, p25
# (FIXME: Further down, in table, a systematic error is quoted as well 
# as stat. error; should really treat this differently)
y_mean = -1e-6
y_std = 6e-6


# Fit exponential to SPT KSZ likelihood
fitfn = lambda p: l_ksz - np.exp(-(D_ksz/p[0])**2.)
p0 = [3.]
pp = scipy.optimize.leastsq(fitfn, p0)[0]
print "Best-fit exp:", pp


# Plot cumulative likelihood function
P.subplot(211)
csum = np.concatenate(([0.], np.cumsum(l_ksz)/np.sum(l_ksz)))
P.plot(D_ksz, csum[:-1], 'b-')
P.plot(D_ksz, csum[:-1], 'b+')
P.axhline(0.95, ls='dotted')
P.axvline(5.7, ls='dotted', color='r')

P.subplot(212)
P.plot(D_ksz, l_ksz, 'k-', lw=1.3)
P.plot(D_ksz, np.exp(-(D_ksz/pp[0])**2.), 'r--', lw=1.3)
P.yscale('log')
P.show()

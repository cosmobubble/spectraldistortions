#!/usr/bin/python
"""
Plot angular diameter distance to last scattering as a 
function of spatial curvature, for fixed Omega_m.
"""

import numpy as np
import cosmolopy as cp
import pylab as P
import scipy.interpolate
from matplotlib import ticker
import distortions

cosmo = distortions.DEFAULT_COSMO
Z_LSS = 1090.79 # Redshift of last-scattering surface

# Define parameter space
phi0 = np.logspace(np.log10(2e-5), np.log10(2e-3), 45)
r0 = np.logspace(np.log10(1e2), np.log10(1.2e4), 30)
r0 = np.concatenate((r0, np.linspace(2100., 2600., 50))) # More samples around null
r0.sort()

def da_fractional_change(ok, om):
    """
    Fractional change in redshift to last scattering, as a function 
    of Omega_k, for some choice of Omega_M.
    """
    # Set initial values for densities etc.
    cosmo['omega_M_0'] = om
    cosmo['omega_k_0'] = 0.
    cosmo['omega_lambda_0'] = 1. - om
    
    # Get Omega_k = 0 value
    d0 = cp.distance.comoving_distance(Z_LSS, z0=0., **cosmo) / (1.+Z_LSS) 

    # Scan through Omega_k space
    f = []
    for _ok in ok:
      cosmo['omega_k_0'] = _ok
      cosmo['omega_lambda_0'] = 1. - _ok - cosmo['omega_M_0']
      d = cp.distance.comoving_distance(Z_LSS, z0=0., **cosmo) / (1.+Z_LSS)
      f.append((d-d0)/d0)
    return np.array(f)

def parameter_constraints_for_omegak(r0, phi0, Y, ok, om):
    """
    Find the parameters (r0, phi0) corresponding to the delta(d_A) 
    that would be obtained for a given value of Omega_K.
    """
    # Get fractional change in da corresponding to some Omega_K
    deltak_p, deltak_m = da_fractional_change([ok, -ok], om)
    
    # Loop through r0; interpolate to find coords corresponding to delta_da
    phival = []; phival2 = [];
    for i in range(r0.size):
      # Deal with different senses of monotonicity (increasing/decreasing)
      if Y.T[i][0] < Y.T[i][-1]:
        _phi0 = scipy.interpolate.interp1d(Y.T[i], phi0, kind='cubic', bounds_error=False)
        phival.append( _phi0(deltak_m) )
        phival2.append(np.nan)
      else:
        _phi0 = scipy.interpolate.interp1d(-Y.T[i], phi0, kind='cubic', bounds_error=False)
        phival.append(np.nan)
        phival2.append( _phi0(-deltak_p) )
    return np.array(phival), np.array(phival2)


########################################################################
# Fractional distance change, as a function of r0
########################################################################
dd = []
terms = []
for _r0 in r0:
  m = distortions.Model(phi0=1., r0=_r0, cosmo=cosmo)
  dl0 = cp.distance.comoving_distance(Z_LSS, z0=0., **cosmo) * (1.+Z_LSS)
  #dl, delta, _terms = distortions.dL_perturbed(Z_LSS, m)
  delta = distortions.dA_perturbed(Z_LSS, m)
  #print _r0, delta, (dl+dl0)/dl0
  print _r0, delta
  dd.append(delta)
  #terms.append(_terms)
dd = np.array(dd)
#terms = np.array(terms).T

########################################################################
# Construct 2D plot from 1D values
########################################################################
R0, PHI0 = np.meshgrid(r0, phi0)
Y = PHI0 * np.array( [dd,]*phi0.size )

# Set-up plot properties
MP_LINEWIDTH = 2.4
MP_TICKSIZE = 10.
P.rc('axes', linewidth=MP_LINEWIDTH)

########################################################################
# Output (phi0, r0) bounds for specific values of Omega_k
########################################################################

phival, phival2 = parameter_constraints_for_omegak(r0, phi0, Y, 1e-3, 0.266)
pp1, pp2 = parameter_constraints_for_omegak(r0, phi0, Y, 2e-4, 0.266)
np.save("curvature-constraints-r0-phi0", (r0, phival, phival2, pp1, pp2) )

# Find value of r0 where things go to zero
#idx = (np.abs(dd - 0.)).argmin()
r0zero = scipy.interpolate.interp1d(dd, r0, kind='linear')(0.)

# Plot y-distortion contours
Y[np.where(np.abs(Y)<1.01e-6)] = 0. # Set to zero to suppress 10^-6 contour
P.subplot(111)

ctr = P.contour(R0, PHI0, Y, locator=ticker.LogLocator(), colors='k', linewidths=3.0)
ctr2 = P.contour(R0, PHI0, -Y, locator=ticker.LogLocator(), colors='k', linewidths=3.0, linestyles='dashed')
#ctr = P.contour(R0, PHI0, Y, colors='k', linewidths=3.0)
#ctr2 = P.contour(R0, PHI0, -Y, colors='k', linewidths=3.0, linestyles='dashed')

P.clabel(ctr, inline=False, fontsize=1., fmt=ticker.NullFormatter())
P.clabel(ctr2, inline=False, fontsize=1., fmt=ticker.NullFormatter())
#P.clabel(ctr, inline=1, fontsize=20., fmt=ticker.LogFormatterMathtext())
#P.clabel(ctr2, inline=1, fontsize=20., fmt=ticker.LogFormatterMathtext())

P.plot(r0, phival, 'b-', lw=5.0)
P.plot(r0, phival2, 'b--', lw=5.0)
#P.plot(r0, phival2, 'r-', lw=5.0)
P.axvline(r0zero, lw=2., color='k', ls='dotted')
P.xscale('log')
P.yscale('log')
P.xlabel("$r_0 \mathrm{[Mpc]}$", fontdict={'fontsize':'28'})#, labelpad=-15.)
P.ylabel("$\Phi_0$", fontdict={'fontsize':'28'})

# Set font sizes
fontsize = 20.
for tick in P.gca().yaxis.get_major_ticks():
  tick.label1.set_fontsize(fontsize)
  tick.tick1line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick2line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick1line.set_markersize(MP_TICKSIZE)
  tick.tick2line.set_markersize(MP_TICKSIZE)
for tick in P.gca().yaxis.get_minor_ticks():
  tick.tick1line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick2line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick1line.set_markersize(0.5*MP_TICKSIZE)
  tick.tick2line.set_markersize(0.5*MP_TICKSIZE)
for tick in P.gca().xaxis.get_major_ticks():
  tick.label1.set_fontsize(fontsize)
  tick.tick1line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick2line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick1line.set_markersize(MP_TICKSIZE)
  tick.tick2line.set_markersize(MP_TICKSIZE)
for tick in P.gca().xaxis.get_minor_ticks():
  tick.tick1line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick2line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick1line.set_markersize(0.5*MP_TICKSIZE)
  tick.tick2line.set_markersize(0.5*MP_TICKSIZE)

P.show()

#!/usr/bin/python
"""
Plot KSZ power spectrum at l=3000, using Zibin & Moss units;
T_0^2 l(l+1) C_l/2pi [uK^2]
"""

import numpy as np
import cosmolopy as cp
import pylab as P
from matplotlib import ticker
import scipy.interpolate
import distortions
import time
tstart = time.time()

PRELOAD = True
cosmo = distortions.DEFAULT_COSMO
T0 = distortions.T0
l = 3000.
fac = l*(l+1)*(T0*1e6)**2. / (2.*np.pi)

# Define parameter space
r0 = np.logspace(np.log10(1e2), np.log10(1.2e4), 25)
phi0 = np.logspace(np.log10(2e-5), np.log10(2e-3), 45)

#m = distortions.Model(phi0=1e0, r0=1e3, cosmo=cosmo)
#print "LINEAR:", distortions.Cl_ksz(m, l=3000., use_harmonic_integral=True)
#print "NONLIN:", distortions.Cl_ksz_nonlin(m, l=3000., use_harmonic_integral=True)

if PRELOAD:
  cl = np.load("data-cl-ksz-nonlin.npy")
else:
  # Scan through r0 values
  cl = []
  for _r0 in r0:
    m = distortions.Model(phi0=1e0, r0=_r0, cosmo=cosmo, use_full_dipole=True)
    _cl = distortions.Cl_ksz_nonlin(m, l=3000., use_harmonic_integral=True)
    cl.append(_cl)
  cl = np.array(cl) * fac
  np.save("data-cl-ksz-nonlin", cl)

# Construct 2D plot from 1D values
R0, PHI0 = np.meshgrid(r0, phi0)
CL = PHI0**2. * np.array( [cl,]*phi0.size )
#np.save('test-cls', CL)

# Set-up plot properties
MP_LINEWIDTH = 2.4
MP_TICKSIZE = 10.
P.rc('axes', linewidth=MP_LINEWIDTH)

# Interpolate to find coords of some preferred value
val = 6.7 # uK, Reichardt et al 2012, Table 3, SPT 95% CL for most conservative(?) model.
phival = []
for i in range(r0.size):
  _phi0 = scipy.interpolate.interp1d(CL.T[i], phi0, kind='cubic', bounds_error=False)
  phival.append( _phi0(val) )
phival = np.array(phival)

# Load distance constraints from plot_lss_distance_omegak.py
d_r0, d_phival, d_phival2, d_pp1, d_pp2 = np.load("curvature-constraints-r0-phi0.npy")

# Plot KSZ contours
P.subplot(111)
ctr = P.contour(R0, PHI0, CL, locator=ticker.LogLocator(), colors='k', linewidths=3.0)
#P.clabel(ctr, inline=1, fontsize=20., fmt='%1.1f')
P.clabel(ctr, inline=1, fontsize='small', fmt=ticker.NullFormatter())
P.plot(r0, phival, 'r-', lw=5.0)
P.xscale('log')
P.yscale('log')
P.xlabel("$r_0 \mathrm{[Mpc]}$", fontdict={'fontsize':'28'}) #, labelpad=-15.)
P.ylabel("$\Phi_0$", fontdict={'fontsize':'28'})

#P.plot(d_r0, d_phival, color='0.5', ls='solid', lw=4.)
#P.plot(d_r0, d_phival2, color='0.5', ls='solid', lw=4.)
#P.plot(d_r0, d_pp1, color='0.5', ls='solid', lw=4.)
#P.plot(d_r0, d_pp2, color='0.5', ls='solid', lw=4.)

# Set font sizes
fontsize = 20.
for tick in P.gca().yaxis.get_major_ticks():
  tick.label1.set_fontsize(fontsize)
  tick.tick1line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick2line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick1line.set_markersize(MP_TICKSIZE)
  tick.tick2line.set_markersize(MP_TICKSIZE)
for tick in P.gca().yaxis.get_minor_ticks():
  tick.tick1line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick2line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick1line.set_markersize(0.5*MP_TICKSIZE)
  tick.tick2line.set_markersize(0.5*MP_TICKSIZE)
for tick in P.gca().xaxis.get_major_ticks():
  tick.label1.set_fontsize(fontsize)
  tick.tick1line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick2line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick1line.set_markersize(MP_TICKSIZE)
  tick.tick2line.set_markersize(MP_TICKSIZE)
for tick in P.gca().xaxis.get_minor_ticks():
  tick.tick1line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick2line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick1line.set_markersize(0.5*MP_TICKSIZE)
  tick.tick2line.set_markersize(0.5*MP_TICKSIZE)

print "Run took", round(time.time() - tstart, 1), "sec."
P.show()

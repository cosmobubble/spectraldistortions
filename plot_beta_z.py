#!/usr/bin/python
"""
Plot beta(z_fid) as fn. of r0, phi0.
"""

import numpy as np
import cosmolopy as cp
import pylab as P
from matplotlib import ticker
import scipy.interpolate
import distortions

cosmo = distortions.DEFAULT_COSMO

Z_FID = 0.5 # Fiducial redshift
Z_FID2 = 0.2 # Fiducial redshift
C = 3e5

# Define parameter space
r0 = np.logspace(2.5, np.log10(1.2e4), 15)
phi0 = np.logspace(-4., -2., 45)

# Scan through r0 values
y = []; y2 = []
for _r0 in r0:
  m = distortions.Model(phi0=1e0, r0=_r0, cosmo=cosmo)
  _y = scipy.interpolate.interp1d(m.z, m.Beta, kind='linear')(Z_FID)
  _y2 = scipy.interpolate.interp1d(m.z, m.Beta, kind='linear')(Z_FID2)
  print _y
  y.append(-_y*C)
  y2.append(-_y2*C)
y = np.array(y)
y2 = np.array(y2)

# Construct 2D plot from 1D values
R0, PHI0 = np.meshgrid(r0, phi0)
Y = PHI0 * np.array( [y,]*phi0.size )
Y2 = PHI0 * np.array( [y2,]*phi0.size )

# Set-up plot properties
MP_LINEWIDTH = 2.4
MP_TICKSIZE = 10.
P.rc('axes', linewidth=MP_LINEWIDTH)

# Plot y-distortion contours
P.subplot(111)
ctr = P.contour(R0, PHI0, Y, locator=ticker.LogLocator(), colors='k', linewidths=3.0)
ctr2 = P.contour(R0, PHI0, Y2, locator=ticker.LogLocator(), colors='r', linewidths=3.0)
#P.clabel(ctr, inline=1, fontsize=20., fmt='%.1e')
P.clabel(ctr, inline=1, fontsize=20., fmt=ticker.LogFormatterMathtext())
#P.clabel(ctr2, inline=1, fontsize=20., fmt=ticker.LogFormatterMathtext())

P.xscale('log')
P.yscale('log')
P.xlabel("$r_0 \mathrm{[Mpc]}$", fontdict={'fontsize':'28'}, labelpad=-15.)
P.ylabel("$\Phi_0$", fontdict={'fontsize':'28'})

# Set font sizes
fontsize = 20.
for tick in P.gca().yaxis.get_major_ticks():
  tick.label1.set_fontsize(fontsize)
  tick.tick1line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick2line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick1line.set_markersize(MP_TICKSIZE)
  tick.tick2line.set_markersize(MP_TICKSIZE)
for tick in P.gca().yaxis.get_minor_ticks():
  tick.tick1line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick2line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick1line.set_markersize(0.5*MP_TICKSIZE)
  tick.tick2line.set_markersize(0.5*MP_TICKSIZE)
for tick in P.gca().xaxis.get_major_ticks():
  tick.label1.set_fontsize(fontsize)
  tick.tick1line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick2line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick1line.set_markersize(MP_TICKSIZE)
  tick.tick2line.set_markersize(MP_TICKSIZE)
for tick in P.gca().xaxis.get_minor_ticks():
  tick.tick1line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick2line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick1line.set_markersize(0.5*MP_TICKSIZE)
  tick.tick2line.set_markersize(0.5*MP_TICKSIZE)

P.show()

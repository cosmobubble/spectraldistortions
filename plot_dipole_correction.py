#!/usr/bin/python
"""
Plot comparison between the dipole approximation and the full dipole expression.
"""

import numpy as np
import pylab as P
import distortions

frac = []
r0 = np.linspace(2e3, 1e4, 5)
for rr in r0:
  print rr
  m = distortions.Model(phi0=1., r0=rr)
  dipole_full = np.array([distortions.dipole_full(m, zz) for zz in m.z])
  frac.append(m.Beta/dipole_full)

P.subplot(111)
for i in range(r0.size):
  P.plot(m.z, frac[i], lw=1.3, label="r0="+str(round(r0[i], 0))+" Mpc")
P.xlim((0., 10.))
P.axhline(0., ls='dotted', color='k')
P.axhline(1., ls='dotted', color='k')
P.legend(loc='upper left', prop={'size':'small'})
P.xlabel("z")
P.ylabel("Beta(approx.)/Beta(full)")
#P.ylim((-0.5, 0.1))
P.show()

#!/usr/bin/python
"""
Plot comparison between the dipole approximation and the full dipole expression.
"""

import numpy as np
import pylab as P
import distortions

frac = []
r0 = 5e3
m = distortions.Model(phi0=1., r0=r0)

print np.shape(m.r)
vel = distortions.velocity(m.r, m.z, m)
dipole_full = np.array([distortions.dipole_full(m, zz) for zz in m.z])


P.subplot(111)
P.plot(m.z, dipole_full, 'k-', lw=1.3)
P.plot(m.z, -m.Beta, 'r-', lw=1.3)
P.plot(m.z, vel/3e5, 'b-', lw=1.3)

P.xlim((0., 10.))
P.xlabel("z")
P.ylabel("Beta")

"""
P.subplot(111)
P.plot(m.z, (distortions.psi(m.r, m.z, m)), 'k-', label="psi" )
P.plot(m.z, (distortions.psi_grad(m.r, m.z, m)), 'r-', label="grad" )
P.plot(m.z, (distortions.psi_laplacian(m.r, m.z, m)), 'g-', label="lapl" )
P.plot(m.z, (distortions.psi_dot(m.r, m.z, m)), 'b-', label="dot" )
P.plot(m.z, (distortions.psi_ddot(m.r, m.z, m)), 'c-', label="ddot" )

P.plot(m.z, -(distortions.psi(m.r, m.z, m)), 'k--')
P.plot(m.z, -(distortions.psi_grad(m.r, m.z, m)), 'r--')
P.plot(m.z, -(distortions.psi_laplacian(m.r, m.z, m)), 'g--')
P.plot(m.z, -(distortions.psi_dot(m.r, m.z, m)), 'b--')
P.plot(m.z, -(distortions.psi_ddot(m.r, m.z, m)), 'c--')

P.xlim((0., 10.))
P.yscale('log')
P.legend(loc='lower right', prop={'size':'x-small'})
"""
P.show()

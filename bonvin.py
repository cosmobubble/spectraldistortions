#!/usr/bin/python
"""
   Implements the full linear perturbative expression for the 
   luminosity distance from Bonvin, Durrer and Gasparini 
   (arXiv:astro-ph/0511183v5).
"""

# NOTE: This code is now obsolete! It just contains a lot of plotting 
# code that I want to keep around. Use distortions.py instead!

import numpy as np
import scipy.integrate
import scipy.interpolate
from scipy.special import jn
import cosmolopy as cp
import pylab as P
import cProfile
import time
tstart = time.time()


# Define default cosmology
DEFAULT_COSMO = {
 'omega_M_0' : 0.27,
 'omega_lambda_0' : 0.73,
 'omega_k_0' : 0.0,
 'omega_b_0' : 0.045,
 'omega_n_0' : 0.0,
 'omega_r_0' : 0.001, # FIXME: radiation density
 'N_nu' : 0,
 'Y_He' : 0.24, # Helium fraction
 'h' : 0.7,
 'n' : 0.96,
 'sigma_8' : 0.8,
 'baryonic_effects': False,
 'gamma': 0.55,
 'r0': 2e3,   # Length scale of potential
 'phi0': 1e0, # Potential amplitude
 'tau0': 0., # Optical depth due to baryons today
 'z_re': 10. # Redshift of reionisation (instantaneous)
}

Z_LSS = 1090. # Redshift of last-scattering surface
N_TAU_SAMPLES = 500 # No. of optical depth history Tau(z) samples
N_LCONE_SAMPLES = 300 # No. sample coordinate points along lightcones
T0 = 2.725 # CMB monopole, in K

########################################################################
# Geodesics
########################################################################

class LightCone(object):
    def __init__(self, zo, zs, nsamples=N_LCONE_SAMPLES, cosmo=DEFAULT_COSMO):
        """
        Calculate (background) null geodesic coordinates for the whole
        lightcone of a remote observer.
         * zo and zs are observer and source redshifts
         * nsamples is the number of samples to pre-calculate
        """
        # TODO: Manually add higher density of samples around r=0.
        # This helps with integrals of the form (grad Psi dot n), 
        # where there is a sign change around z=0 (can then reduce nsamples)
        
        # Store input arguments
        self.zs = zs; self.zo = zo
        self.nsamples = nsamples
        
        # Observer position
        ro = eta(zo); self.ro = ro
        
        # Pre-calculate geodesic points (z is used as time coord.)
        if zo > 1e-5: # better sampling in log space
            self.zt = np.logspace(np.log10(zo), np.log10(zs), nsamples)
        else:
            self.zt = np.concatenate( ([0.], np.logspace(-5., 
                                       np.log10(zs), nsamples)) )
        self.eta = eta(self.zt)
        rr = cp.distance.comoving_distance(self.zt, z0=zo, **cosmo)
        
        # Comoving distances along inbound/outbound legs
        self.r_in = ro - rr
        self.r_out = ro + rr
        

########################################################################
# Background and perturbative quantities
########################################################################

def omegaM(z, cosmo=DEFAULT_COSMO):
	"""Evolving matter fraction, Omega_M(a), from Linder (nr. Eq. 5),
	   arXiv:astro-ph/0507263 (2005)."""
	a = 1. / (1. + z)
	return cosmo['Om'] * a**(-3.) / cp.distance.e_z(z, **cosmo)**2.

def fg(z, cosmo=DEFAULT_COSMO):
	"""Linear growth rate, parameterised as in Linder."""
	if 'gamma' in cosmo.keys():
		gamma = cosmo['gamma']
	return omegaM(z, cosmo)**gamma

def H(z, cosmo=DEFAULT_COSMO):
    """
       Dimensionless Hubble rate as fn. of redshift, including 
       radiation term.
    """
    x = 1.+z; p = cosmo
    return p['h'] * np.sqrt( p['omega_r_0']*x**4. + p['omega_M_0']*x**3. \
                            + p['omega_k_0']*x**2. + p['omega_lambda_0'] )

def eta_direct(z, cosmo=DEFAULT_COSMO):
    """
       Conformal time at some background redshift z. Use the relation 
       between comoving distance and conformal time: eta = chi / c
    """
    # FIXME: Need to check units (here, set c=1)
    return cp.distance.comoving_distance(z, z0=0., **cosmo)


########################################################################
# Definition of the Newtonian potential, and derivatives of it
########################################################################

def delta(r, zt, cosmo=DEFAULT_COSMO):
    """Density fluctuation from the Poisson equation."""
    # del^2 Psi = Psi'' + 2/r Psi' = 4 pi G <rho> a^2 delta
    # Units of Mpc^-2 already folded into density expression
    delta = psi_laplacian(r, zt, cosmo)
    delta /= 1.5 * cosmo['omega_M_0'] * (1.+zt) * (100.*cosmo['h']/3e5)**2.
    return delta

def psi(r, zt, cosmo=DEFAULT_COSMO):
    """
    Newtonian potential Psi(r, zt), where zt is background redshift 
    as time coord.
    """
    fr = np.exp(-(np.abs(r)/cosmo['r0'])**2.)
    D = cp.perturbation.fgrowth(zt, cosmo['omega_M_0'])
    return D * cosmo['phi0'] * fr

def psi_grad(r, zt, cosmo=DEFAULT_COSMO):
    """Gradient of Psi(eta), which can be calculated analytically."""
    # Assumes spherical symmetry; should be vector quantity
    r0 = cosmo['r0']
    fr = -2.*np.abs(r)/r0**2. * np.exp(-(np.abs(r)/r0)**2.)
    D = cp.perturbation.fgrowth(zt, cosmo['omega_M_0'])
    return D * cosmo['phi0'] * fr

def psi_laplacian(r, zt, cosmo=DEFAULT_COSMO):
    """Laplacian of Psi(eta), which can be calculated analytically."""
    r0 = cosmo['r0']
    fr = (-2./r0**2.) * np.exp(-(np.abs(r)/r0)**2.) * (2.*(r/r0)**2. - 3.)
    D = cp.perturbation.fgrowth(zt, cosmo['omega_M_0'])
    return D * cosmo['phi0'] * fr

# FIXME below this...

def psi_dot(eta, cosmo=DEFAULT_COSMO):
    """Proper-time derivative of Psi(eta)."""
    f = 1. # FIXME: Linear growth rate
    return f * psi(eta, cosmo)

def psi_ddot(eta, cosmo=DEFAULT_COSMO):
    """Proper-time second derivative of Psi(eta)."""
    f = 1. # FIXME: Need time derivative of linear growth rate
    return f * psi(eta, cosmo)
    

########################################################################
# Distance/redshift integrals
########################################################################

def dL_perturbed(eta_s, cosmo=DEFAULT_COSMO):
    """
       Perturbed luminosity distance dL(eta_s), from Eq. 59 of Bonvin et 
       al., where eta_s is the conformal time at the source.
    """
    # FIXME: Missing peculiar velocity terms. Shouldn't these be added 
    # in, since the potential will cause a peculiar velocity?
    
    # Source/observer redshift, conformal time, and peculiar potential
    zo = 0.; zs = z_eta(eta_s)
    eta_o = eta(zo)
    psi_o = psi(eta_o); psi_s = psi(eta_s)
    
    # mathcal{H} = H * a, defined below Eq. 55 in Bonvin et al.
    _Hs = H(zs) / (1. + zs)
    
    # Background dL(z) term
    dL = (1. + zs) * (eta_o - eta_s)
    
    # Gravitational redshift terms (line 2 of Eq. 59)
    zgrav1 = ( ( 1./(_Hs*(eta_o - eta_s)) ) - 1. ) * psi_s
    zgrav2 = -1. * psi_o / ( (eta_o - eta_s) * _Hs )
    
    # Integrated LOS terms (line 3 of Eq. 59)
    integ3 = lambda n: ((n-eta_s)/(eta_o-eta_s))*psi_dot(n, cosmo)
    integ4 = lambda n: ((n-eta_s)*(eta_o-n)/(eta_o-eta_s))*psi_ddot(n, cosmo)
    y1, e1 = scipy.integrate.quad(psi, eta_o, eta_s, args=(cosmo,))
    y2, e2 = scipy.integrate.quad(psi_dot, eta_o, eta_s, args=(cosmo,))
    y3, e3 = scipy.integrate.quad(integ3, eta_o, eta_s)
    y4, e4 = scipy.integrate.quad(integ4, eta_o, eta_s)
    
    los1 = 2. * y1 / (eta_o - eta_s)
    los2 = 2. * y2 / ( (eta_o - eta_s) * _Hs )
    los3 = -2. * y3
    los4 = y4
    
    # Lensing term
    integ5 = lambda n: ((n-eta_s)*(eta_o-n)/(eta_o-eta_s))*psi_laplacian(n, cosmo)
    y5, e5 = scipy.integrate.quad(integ5, eta_o, eta_s)
    lens = -1. * y5
    
    # Return final result
    delta = zgrav1 + zgrav2 + los1 + los2 + los3 + los4 + lens
    return dL * (1. + delta)

def delta_z(eta_o, eta_s, cosmo=DEFAULT_COSMO):
    """Perturbed z_s, from Eq. 57 of Bonvin et al."""
    # FIXME: Missing peculiar velocity term again
    
    # Source/observer redshift, conformal time, and peculiar potential
    zo = z_eta(eta_o); zs = z_eta(eta_s)
    psi_o = psi(eta_o); psi_s = psi(eta_s)
    
    # Integral of potential gradient
    y1 = [scipy.integrate.quad(psi_grad, eta_o, ee, args=(cosmo,))[0] for ee in eta_s]
    y1 = np.array(y1)
    
    # Take dot product with LOS (assumed parallel to radial direction)
    sgn = +1. # or -1.
    
    # Return result
    delta_z = (1. + zs) * ( psi_o - psi_s + 2.*sgn*y1 )
    return delta_z

def beta(z, cosmo=DEFAULT_COSMO):
    """
       Calculate an approximation to the dipole as seen by an observer 
       at a given point down our past lightcone. Uses Eq. 57 of Bonvin.
    """
    # Solve null geodesic eqn. on lightcone (+/- r directions)
    l = LightCone(zo=z, zs=Z_LSS, cosmo=cosmo)
    
    # Get potentials at off-centre observer, and source in in/outbound directions
    psi_o = psi(l.r_in[0], l.zt[0])
    psi_s_in = psi(l.r_in[-1], l.zt[-1])
    psi_s_out = psi(l.r_out[-1], l.zt[-1])
    
    # Integral of potential gradient (inbound and outbound) from discrete samples
    psi_grad_in = -1. * np.sign(l.r_in) * psi_grad(l.r_in, l.zt, cosmo=cosmo)
    psi_grad_out = psi_grad(l.r_out, l.zt, cosmo=cosmo)
    y_in = scipy.integrate.simps(psi_grad_in, l.eta)
    y_out = scipy.integrate.simps(psi_grad_out, l.eta)
    
    # Get beta = delta_z / (1+z_in + 1 + z_out) in each direction (for z_in ~ z_out).
    beta = (psi_s_out - psi_s_in) - 2.* (y_in - y_out)
    return beta # psi_s_out, psi_s_in, y_out, y_in
    

########################################################################
# SZ/y-distortion integrals
########################################################################

def tau_history(cosmo=DEFAULT_COSMO):
    """
    Model for the optical depth as a function of redshift, from Eq. 18 
    of Zibin & Moss, arXiv:1105.0909.
    """
    # Scaling constants
    fb = cosmo['omega_b_0'] / cosmo['omega_M_0'] # Baryon fraction
    YHe = cosmo['Y_He']; h = cosmo['h']; om = cosmo['omega_M_0']
    alpha = 3.45e-2 * h * fb * om * (2. - YHe) # Pre-factor
    
    # Integrate to find Tau(z), with instantaneous reionisation approx.
    dtaudz = lambda _x, zz: (zz < cosmo['z_re']) * (1. + zz)**2. \
                             / cp.distance.e_z(zz, **cosmo)
    z = np.concatenate( ([0.], np.logspace(-5, np.log10(Z_LSS), N_TAU_SAMPLES)) )
    tau = alpha * scipy.integrate.odeint(dtaudz, cosmo['tau0'], z)
    return tau, z

def dtau_dz(z, cosmo=DEFAULT_COSMO):
    """Optical depth derivative, from Zibin & Moss Eq. 18."""
    # Scaling constants
    fb = cosmo['omega_b_0'] / cosmo['omega_M_0'] # Baryon fraction
    YHe = cosmo['Y_He']; h = cosmo['h']; om = cosmo['omega_M_0']
    alpha = 3.45e-2 * h * fb * om * (2. - YHe) # Pre-factor
    
    # Return dTau/dz with instantaneous reionisation approx.
    return alpha * (z < cosmo['z_re']) * (1. + z)**2. \
                             / cp.distance.e_z(z, **cosmo)

def kineticSZ(cosmo=DEFAULT_COSMO):
    """Kinetic SZ DeltaT/T, from Zibin & Moss Eq. 5."""
    
    # Redshift range for integration (out to reionisation)
    z = np.linspace(0., cosmo['z_re'], 80)
    r = cp.distance.comoving_distance(z, z0=0., **cosmo)
    
    # Sample fns in integrand for range of redshift
    dtdz = dtau_dz(z, cosmo)
    _beta = np.array([beta(zz, cosmo) for zz in z])
    _delta = delta(r, z, cosmo)
    
    # Integrate samples
    dT = scipy.integrate.simps(dtdz * _beta * _delta, z)
    return dT

def Cl_ksz(l=3000., cosmo=DEFAULT_COSMO):
    """
    Compute angular power spectrum coefficient for kinetic SZ, using 
    the Limber approximation, as set out in Eq. 29 of Zibin & Moss.
    """
    
    # Redshift range for integration (out to reionisation)
    z = np.linspace(0., cosmo['z_re'], 50) # FIXME: Accuracy?
    r = cp.distance.comoving_distance(z, z0=0., **cosmo)
    
    # Scaling constants for optical depth
    fb = cosmo['omega_b_0'] / cosmo['omega_M_0'] # Baryon fraction
    YHe = cosmo['Y_He']; h = cosmo['h']
    om = cosmo['omega_M_0']; ok = cosmo['omega_k_0']
    c = 3e5 # Speed of light, km/s
    alpha = 1.15e-5 * fb * om * h**2. * (2. - YHe) # Pre-factor of dTau/dr, in Mpc^-1
    
    # Matter power spectrum down past light-cone
    # TODO: Use non-linear matter power instead (see Zibin & Moss, Fig. 2)
    k = 0.5*(2.*l + 1.) / r # k [Mpc^-1], r [Mpc]. FIXME: Check if h^-1 Mpc or not
    k[np.where(np.isinf(k))] = 1e7 # Remove inf, replace with large number
    
    Pk0 = cp.perturbation.power_spectrum(k, 0.0, **cosmo)
    D = cp.perturbation.fgrowth(z, cosmo['omega_M_0'])
    Pk = Pk0 * D**2.
    #Pk[0] = 0.; Pk0[0] = 0. # Get BCs correct
    
    # Dipole
    Beta = np.array([beta(zz, cosmo) for zz in z])
    
    # Perform integration (1)
    dtaudr2 = np.abs((1.+z)**4./(1. - ok*(100.*h*r/c)**2.) ) # Multiply by alpha afterwards
    integ = dtaudr2 * Beta**2. * Pk * r
    Cl = scipy.integrate.simps(integ, r)
    Cl *= alpha**2. * 16.*np.pi**2. / (2.*l + 1.)**3.
    
    # Perform integration (2)
    integ = dtaudr2 * Beta**2. * Pk / k**3.
    Clk = scipy.integrate.simps(integ, k)
    Clk *= alpha**2. * 4.*np.pi**2. / (2.*l + 1.)
    
    # Fix k array and return
    k[0] = np.nan
    return Cl, dtaudr2*Beta**2.*Pk*r, r, z, Pk, k, Pk0, Clk
    

def ydistortion(cosmo=DEFAULT_COSMO):
    """
    Calculate the Compton y-distortion parameter, defined in Eq. 39 of 
    Zibin & Moss.
    """
    # Redshift range for integration (out to reionisation)
    #z = np.linspace(0., cosmo['z_re'], 50) # FIXME: Accuracy?
    #r = cp.distance.comoving_distance(z, z0=0., **cosmo)
    
    # Scaling constants for optical depth
    fb = cosmo['omega_b_0'] / cosmo['omega_M_0'] # Baryon fraction
    YHe = cosmo['Y_He']; h = cosmo['h']
    om = cosmo['omega_M_0']; ok = cosmo['omega_k_0']
    c = 3e5 # Speed of light, km/s
    alpha = 1.15e-5 * fb * om * h**2. * (2. - YHe) # Pre-factor of dTau/dr, in Mpc^-1
    
    # Dipole, and optical depth (multiply by alpha afterwards)
    #Beta = np.array([beta(zz, cosmo) for zz in z])
    dtaudr = (1.+z)**2. / np.sqrt( 1. - ok*(100.*h*r/c)**2. )
    
    # Integrate y-distortion
    integ = dtaudr * Beta**2.
    y = scipy.integrate.simps(integ, r)
    return (7./10.) * alpha * y

def dydr(cosmo=DEFAULT_COSMO):
    """Derivative of the Compton y-distortion parameter."""
    # Redshift range for integration (out to reionisation)
    #z = np.linspace(0., cosmo['z_re'], 50) # FIXME: Accuracy?
    #r = cp.distance.comoving_distance(z, z0=0., **cosmo)
    
    # Scaling constants for optical depth
    fb = cosmo['omega_b_0'] / cosmo['omega_M_0'] # Baryon fraction
    YHe = cosmo['Y_He']; h = cosmo['h']
    om = cosmo['omega_M_0']; ok = cosmo['omega_k_0']
    c = 3e5 # Speed of light, km/s
    alpha = 1.15e-5 * fb * om * h**2. * (2. - YHe) # Pre-factor of dTau/dr, in Mpc^-1
    
    # Dipole, and optical depth (multiply by alpha afterwards)
    #Beta = np.array([beta(zz, cosmo) for zz in z])
    dtaudr = (1.+z)**2. / np.sqrt( 1. - ok*(100.*h*r/c)**2. )
    return (7./10.) * alpha * dtaudr * Beta**2.




def Clksztest(l=3000., cosmo=DEFAULT_COSMO):
    """
    Compute angular power spectrum coefficient for kinetic SZ, using 
    the Limber approximation, as set out in Eq. 29 of Zibin & Moss.
    """
    
    # Redshift range for integration (out to reionisation)
    #z = np.linspace(0., cosmo['z_re'], 50) # FIXME: Accuracy?
    #r = cp.distance.comoving_distance(z, z0=0., **cosmo)
    
    # Scaling constants for optical depth
    fb = cosmo['omega_b_0'] / cosmo['omega_M_0'] # Baryon fraction
    YHe = cosmo['Y_He']; h = cosmo['h']
    om = cosmo['omega_M_0']; ok = cosmo['omega_k_0']
    c = 3e5 # Speed of light, km/s
    alpha = 1.15e-5 * fb * om * h**2. * (2. - YHe) # Pre-factor of dTau/dr, in Mpc^-1
    
    # Matter power spectrum down past light-cone
    # TODO: Use non-linear matter power instead (see Zibin & Moss, Fig. 2)
    k = 0.5*(2.*l + 1.) / r # k [Mpc^-1], r [Mpc]. FIXME: Check if h^-1 Mpc or not
    k[np.where(np.isinf(k))] = 1e8 # Remove inf, replace with large number
    
    Pk0 = cp.perturbation.power_spectrum(k, 0.0, **cosmo)
    D = cp.perturbation.fgrowth(z, cosmo['omega_M_0'])
    Pk = Pk0 * D**2.
    
    # Perform integration (1)
    dtaudr2 = np.abs((1.+z)**4./(1. - ok*(100.*h*r/c)**2.) ) # Multiply by alpha afterwards
    integ = dtaudr2 * Beta**2. * Pk * r
    Cl = scipy.integrate.simps(integ, r)
    Cl *= alpha**2. * 16.*np.pi**2. / (2.*l + 1.)**3.
    
    # Perform integration (2)
    integ = dtaudr2 * Beta**2. * Pk / k**3.
    Clk = scipy.integrate.simps(integ, k)
    Clk *= -1.*alpha**2. * 4.*np.pi**2. / (2.*l + 1.)
    
    # Fix k array and return
    #k[0] = np.nan
    return Cl, dtaudr2*Beta**2.*Pk*r, r, z, Pk, k, Pk0, Clk, dtaudr2 * Beta**2. * Pk / k**3., dtaudr2, Beta**2., np.cumsum(integ[::-1])




########################################################################
# Set-up problem
########################################################################

cosmo = DEFAULT_COSMO

# Precompute eta(z)
_z = np.concatenate( ([0.], np.logspace(-5, np.log10(Z_LSS*1.01), 100)) )
_eta = cp.distance.comoving_distance(_z, z0=0., **cosmo)
eta = scipy.interpolate.interp1d(_z, _eta, kind='linear')

"""
# Test output
z = np.logspace(-5., np.log10(cosmo['z_re']), 300) # FIXME: Accuracy?
z = np.concatenate(([0.], z))
r = cp.distance.comoving_distance(z, z0=0., **cosmo)
Beta = np.array([beta(zz, cosmo) for zz in z])
np.save( "ksztest-500Mpc", np.vstack((z, r, Beta)) )
print "\nRun took", round(time.time() - tstart, 1), "sec."
"""
"""
z = np.logspace(-5., np.log10(cosmo['z_re']), 300) # FIXME: Accuracy?
z = np.concatenate(([0.], z))
r = cp.distance.comoving_distance(z, z0=0., **cosmo)
Beta = np.array([beta(zz, cosmo) for zz in z])
"""

P.subplot(111)

z, r, Beta = np.load("ksztest-500Mpc.npy")
print "y =", ydistortion()
P.plot(z, dydr(), lw=1.3, label="r_0 = 0.5 Gpc")

z, r, Beta = np.load("ksztest.npy")
print "y =", ydistortion()
P.plot(z, dydr(), lw=1.3, label="r_0 = 2 Gpc")

z, r, Beta = np.load("ksztest-10Gpc.npy")
print "y =", ydistortion()
P.plot(z, dydr(), lw=1.3, label="r_0 = 10 Gpc")

P.legend(loc='upper right', prop={'size':'small'})

P.xlabel("z")
P.ylabel("dy/dr")
P.show()

exit()
ell = np.logspace(0., np.log10(3000.), 50)
####
z, r, Beta = np.load("ksztest.npy")
Clr = []; Clk = []
for l in ell:
    Cl, integ, r, z, Pk, k, Pk0, Clkk, integk, dtau, bet, cumsum = Clksztest(l, cosmo)
    Clr.append(Cl)
    Clk.append(Clkk)
Clr = np.array(Clr)
Clk = np.array(Clk)

####
z, r, Beta = np.load("ksztest-500Mpc.npy")
Clr1 = []; Clk1 = []
for l in ell:
    Cl, integ, r, z, Pk, k, Pk0, Clkk, integk, dtau, bet, cumsum = Clksztest(l, cosmo)
    Clr1.append(Cl)
    Clk1.append(Clkk)
Clr1 = np.array(Clr1)
Clk1 = np.array(Clk1)

z, r, Beta = np.load("ksztest-10Gpc.npy")
Clr2 = []; Clk2 = []
for l in ell:
    Cl, integ, r, z, Pk, k, Pk0, Clkk, integk, dtau, bet, cumsum = Clksztest(l, cosmo)
    Clr2.append(Cl)
    Clk2.append(Clkk)
Clr2 = np.array(Clr2)
Clk2 = np.array(Clk2)


P.subplot(111)

P.plot(ell, Clr1, 'g-', lw=1.3, label="r_0 = 0.5 Gpc")
P.plot(ell, Clr, 'r-', lw=1.3, label="r_0 = 2 Gpc")
P.plot(ell, Clr2, 'b-', lw=1.3, label="r_0 = 10 Gpc")

P.xscale('log')
P.yscale('log')
P.xlabel("$\ell$")
P.ylabel("$\ell(\ell + 1)C_\ell / 2 \pi$")

P.legend(loc='upper right', prop={'size':'small'})

P.show()
exit()


"""
P.subplot(111)
P.plot(z2, Beta2, lw=1.3, label="r_0 = 500 Mpc")
P.plot(z, Beta, lw=1.3, label="r_0 = 2 Gpc")
P.plot(z3, Beta3, lw=1.3, label="r_0 = 10 Gpc")
P.axhline(0., ls='dotted', color='k')
P.legend(loc='lower right', prop={'size':'small'})
P.xlabel("z")
P.ylabel("$\\beta(z)$")
P.show()
"""

exit()

ell = np.logspace(0., np.log10(3000.), 2)


Clr = []; Clk = []
for l in ell:
    Cl, integ, r, z, Pk, k, Pk0, Clkk, integk, dtau, bet, cumsum = Clksztest(l, cosmo)
    Clr.append(Cl)
    Clk.append(Clkk)
Clr = np.array(Clr)
Clk = np.array(Clk)


rmax = np.max(r)
"""
I_limber = []
for l in ell:
    kmin = (2.*l + 1.)/rmax
    
    # Limber approximation calculation
    kk = np.logspace(np.log10(kmin), 3., 100)
    pk = cp.perturbation.power_spectrum(kk, 0.0, **cosmo)
    I_limber.append( scipy.integrate.simps(pk, kk) )
"""

#P.subplot(211)
#P.plot(ell, I_limber)
#P.yscale('log')

kk = np.logspace(-5., 3., 100)
pk = cp.perturbation.power_spectrum(kk, 0.0, **cosmo)

# Scaling constants for optical depth
fb = cosmo['omega_b_0'] / cosmo['omega_M_0'] # Baryon fraction
YHe = cosmo['Y_He']; h = cosmo['h']
om = cosmo['omega_M_0']; ok = cosmo['omega_k_0']
c = 3e5 # Speed of light, km/s
alpha = 1.15e-5 * fb * om * h**2. * (2. - YHe) # Pre-factor of dTau/dr, in Mpc^-1

# Window functions, as fn. of scale
P.subplot(111)
for ll in [1., 100., 500., 1000., 3000.]:
	D2 = cp.perturbation.fgrowth(z, cosmo['omega_M_0'])**2.
	dtaudr2 = np.abs((1.+z)**4./(1. - ok*(100.*h*r/c)**2.) )
	k = 0.5*(2.*ll + 1.) / r # k [Mpc^-1], r [Mpc]. FIXME: Check if h^-1 Mpc or not
	k[np.where(np.isinf(k))] = 1e4 # Remove inf, replace with large number
	_pk = cp.perturbation.power_spectrum(k, 0.0, **cosmo)
	
	P.plot(k, Beta**2. * D2 * dtaudr2 * _pk/k**3., label="l="+str(int(ll)), lw=1.3)

P.legend(loc='upper right', prop={'size':'small'})
P.xlabel("k [Mpc^-1]")
P.xscale('log')
P.yscale('log')
P.grid(True)
P.show()

exit()
"""
P.subplot(111)
#P.plot(ell, Clr*ell*(ell+1.)/(2.*np.pi))
P.axhline(0., ls='dotted', color='k')
P.plot(z, Beta)

#P.plot(ell, Clk)
#P.yscale('log')
#P.xscale('log')
#P.xlabel("$\ell$")
#P.ylabel("$\ell(\ell + 1)C_\ell / 2 \pi$")
P.xlabel("z")
P.ylabel("$\\beta(\mathrm{z})$")
P.show()
exit()
"""

P.subplot(111)
#P.plot((2.*ell + 1.)/rmax, ell)
P.plot(kk, pk/kk**3., label="$P(k)/k^3$", alpha=0.8)
P.plot(k, D2, label="$D^2(z)$", alpha=0.8)
P.plot(k, dtaudr2, label="$(d\\tau/dr)^2$", alpha=0.8)
P.plot(k, Beta**2., label=r"$\beta^2(z)$", alpha=0.8)

P.plot(k, Beta**2. * D2 * dtaudr2 * _pk/k**3., 'k-', lw=1.5, label="Integrand")

P.axvline(1e3, color='b')
P.axvline(np.max(0.5*(2.*ell + 1.)/rmax), color='b', ls='dashed')
P.axvline(np.min(0.5*(2.*ell + 1.)/rmax), color='b', ls='dashed')

l = 3000.
k3 = 0.5*(2.*l + 1.) / r # k [Mpc^-1], r [Mpc]. FIXME: Check if h^-1 Mpc or not
k3[np.where(np.isinf(k3))] = 1e8 # Remove inf, replace with large number

l = 100.
k1 = 0.5*(2.*l + 1.) / r # k [Mpc^-1], r [Mpc]. FIXME: Check if h^-1 Mpc or not
k1[np.where(np.isinf(k1))] = 1e8 # Remove inf, replace with large number

l = 2.
k2 = 0.5*(2.*l + 1.) / r # k [Mpc^-1], r [Mpc]. FIXME: Check if h^-1 Mpc or not
k2[np.where(np.isinf(k2))] = 1e8 # Remove inf, replace with large number


#P.plot(k3, 10.*np.ones(k3.size), 'b.')
#P.plot(k1, 100.*np.ones(k1.size), 'r.')
#P.plot(k2, 1000.*np.ones(k2.size), 'g.')
P.xscale('log')
P.yscale('log')
P.legend(loc='upper right', prop={'size':'small'})
P.xlabel("k [Mpc^-1]")

P.show()

exit()







Cl, integ1, r1, z, Pk1, k1, Pk01, Clkk, integk1 = Clksztest(1000., cosmo)
Cl, integ2, r2, z, Pk2, k2, Pk02, Clkk, integk2 = Clksztest(2000., cosmo)
Cl, integ3, r3, z, Pk3, k3, Pk03, Clkk, integk3 = Clksztest(3000., cosmo)

print "Cl =", Cl

P.subplot(211)
#P.plot(r1, Pk1)
#P.plot(r2, Pk2)
#P.plot(r3, Pk3)
P.plot(ell, Clr)
P.plot(ell, Clk)
P.yscale('log')

# Get CLASS P(k)
_k, _pk = np.genfromtxt("/home/phil/postgrad/software/class/output/test_z1_pk.dat").T
_k1, _pk1 = np.genfromtxt("/home/phil/postgrad/software/class/output/test_z3_pk.dat").T
_knl, _pknl = np.genfromtxt("/home/phil/postgrad/software/class/output/test_z1_pk_nl.dat").T
_knl1, _pknl1 = np.genfromtxt("/home/phil/postgrad/software/class/output/test_z3_pk_nl.dat").T

P.subplot(212)
kk = np.logspace(-5., 3., 500)
pk = cp.perturbation.power_spectrum(kk, 0.0, **cosmo)
D = cp.perturbation.fgrowth(z, cosmo['omega_M_0'])
P.plot(kk, pk, 'k-')
P.plot(k1, Pk01, 'r-')
P.plot(k1, Pk1, 'g-')
P.plot(k3, Pk3, 'g--')
P.plot(k1, D**2., 'y-')
P.plot(_k, _pk, 'c--')
P.plot(_knl, _pknl, 'm--')
P.plot(_k1, _pk1, 'c-')
P.plot(_knl1, _pknl1, 'm-')
P.yscale('log')
P.xscale('log')

P.show()









########################################################################
exit()

# Compute kSZ effect
#dT = kineticSZ(cosmo)
Cl0, integ0, r0, z0, pk0, k0, pk00, clk0 = Cl_ksz(3000., cosmo)
Cl1, integ1, r1, z1, pk1, k1, pk01, clk1 = Cl_ksz(2000., cosmo)
Cl2, integ2, r2, z2, pk2, k2, pk02, clk2 = Cl_ksz(1000., cosmo)

print "Cl(l=3000):", Cl0, clk0, Cl0/clk0
print "Cl(l=2000):", Cl1, clk1, Cl1/clk1
print "Cl(l=1000):", Cl2, clk2, Cl2/clk2

P.subplot(221)
P.plot(z0, integ0) # / (2.*3000. + 1.)**3.)
P.plot(z1, integ1) # / (2.*2000. + 1.)**3.)
P.plot(z2, integ2) # / (2.*1000. + 1.)**3.)
P.yscale('log')

# l-dependence: 1 factor of 10x from probing smaller k (larger scales) with smaller l
#                  -> P(k) greater at larger scales
#               1 factor of ~l^3 due to l-weighting of C_l

P.subplot(222)

kk = np.logspace(-4, 2, 200)
pp = cp.perturbation.power_spectrum(kk, 0.0, **cosmo)

P.plot(kk, pp, 'k-')
P.axvline(np.max(k0[1:]), color='b')
P.axvline(np.min(k0[1:]), color='b')
P.axvline(np.max(k1[1:]), color='g')
P.axvline(np.min(k1[1:]), color='g')
P.axvline(np.max(k2[1:]), color='r')
P.axvline(np.min(k2[1:]), color='r')
P.yscale('log')
P.xscale('log')

P.subplot(223)
D = cp.perturbation.fgrowth(z0, cosmo['omega_M_0'])
ppp = cp.perturbation.power_spectrum(k0, 0.0, **cosmo)
ppp2 = cp.perturbation.power_spectrum(k2, 0.0, **cosmo)
P.plot(z0, D**2., 'r-')
P.plot(z0, ppp, 'b-')
P.plot(z2, ppp2, 'b--')
P.plot(z0, ppp*D**2., 'k-')
P.yscale('log')

P.subplot(224)
P.plot(z0, k0)
P.yscale('log')

print "\nRun took", round(time.time() - tstart, 1), "sec."
P.show()





exit()

ell = np.linspace(500., 3000., 5)
Dl = np.array( [Cl_ksz(l, cosmo) for l in ell] ) *ell*(ell+1.)*(T0*1e6)**2./(2.*np.pi)
cosmo['z_re'] = 4.
Dl2 = np.array( [Cl_ksz(l, cosmo) for l in ell] ) *ell*(ell+1.)*(T0*1e6)**2./(2.*np.pi)



P.subplot(111)
P.plot(ell, Dl, label="z_re=10")
P.plot(ell, Dl2, label="z_re=8")
P.xlabel("l")
P.ylabel("T_0^2 D_l [uK^2]")

P.show()

exit()

P.subplot(111)
P.plot(r, dtaudr, 'r-')
P.plot(r, -Beta, 'g-')
P.plot(r, Pk, 'b-')
P.plot(r, r, 'y-')
P.plot(r, Pk*(Beta*dtaudr)**2.*r, 'k-')
P.yscale('log')
P.ylim((1e-2, 1e8))
#P.xlabel("ell")
#P.ylabel("T_0^2 Dl [uK^2]")


print "\nRun took", round(time.time() - tstart, 1), "sec."
#P.show()

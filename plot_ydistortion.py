#!/usr/bin/python
"""
Plot y-distortion parameter as fn. of r0, phi0.
"""
import numpy as np
import cosmolopy as cp
import pylab as P
from matplotlib import ticker
import scipy.interpolate
import distortions
import time
tstart = time.time()

PRELOAD = True
cosmo = distortions.DEFAULT_COSMO

# Define parameter space
r0 = np.logspace(np.log10(1e2), np.log10(1.2e4), 25)
phi0 = np.logspace(np.log10(2e-5), np.log10(2e-3), 45)

if PRELOAD:
  y = np.load("data-ydistortion.npy")
else:
  # Scan through r0 values
  y = []
  for _r0 in r0:
    m = distortions.Model(phi0=1e0, r0=_r0, cosmo=cosmo, use_full_dipole=True)
    _y = distortions.ydistortion(m)
    y.append(_y)
  y = np.array(y)
  np.save("data-ydistortion", y)

# Construct 2D plot from 1D values
R0, PHI0 = np.meshgrid(r0, phi0)
Y = PHI0**2. * np.array( [y,]*phi0.size )

# Set-up plot properties
MP_LINEWIDTH = 2.4
MP_TICKSIZE = 10.
P.rc('axes', linewidth=MP_LINEWIDTH)

# Interpolate to find coords of some preferred value
val = 2e-9 # PIXIE y-distortion upper limit
val2 = 1.5e-5 # COBE/FIRAS y-distortion upper limit

phival = []; phival2 = []
for i in range(r0.size):
  _phi0 = scipy.interpolate.interp1d(Y.T[i], phi0, kind='cubic', bounds_error=False)
  phival.append( _phi0(val) )
  phival2.append( _phi0(val2) )
phival = np.array(phival)
phival2 = np.array(phival2)

# Load distance constraints from plot_lss_distance_omegak.py
d_r0, d_phival, d_phival2, d_pp1, d_pp2 = np.load("curvature-constraints-r0-phi0.npy")

# Plot y-distortion contours
P.subplot(111)
ctr = P.contour(R0, PHI0, Y, locator=ticker.LogLocator(), colors='k', linewidths=3.0)
#P.clabel(ctr, inline=1, fontsize=20., fmt='%.1e')
#P.clabel(ctr, inline=1, fontsize=20., fmt=ticker.LogFormatterMathtext())
P.clabel(ctr, inline=1, fontsize=20., fmt=ticker.NullFormatter())
P.plot(r0, phival, 'b-', lw=5.0)
P.plot(r0, phival2, 'r-', lw=5.0)

#P.plot(d_r0, d_phival, color='0.5', ls='solid', lw=4.)
#P.plot(d_r0, d_phival2, color='0.5', ls='solid', lw=4.)
#P.plot(d_r0, d_pp1, color='0.5', ls='solid', lw=4.)
#P.plot(d_r0, d_pp2, color='0.5', ls='solid', lw=4.)

P.xscale('log')
P.yscale('log')
P.xlabel("$r_0 \mathrm{[Mpc]}$", fontdict={'fontsize':'28'}) #, labelpad=-15.)
P.ylabel("$\Phi_0$", fontdict={'fontsize':'28'})

# Set font sizes
fontsize = 20.
for tick in P.gca().yaxis.get_major_ticks():
  tick.label1.set_fontsize(fontsize)
  tick.tick1line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick2line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick1line.set_markersize(MP_TICKSIZE)
  tick.tick2line.set_markersize(MP_TICKSIZE)
for tick in P.gca().yaxis.get_minor_ticks():
  tick.tick1line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick2line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick1line.set_markersize(0.5*MP_TICKSIZE)
  tick.tick2line.set_markersize(0.5*MP_TICKSIZE)
for tick in P.gca().xaxis.get_major_ticks():
  tick.label1.set_fontsize(fontsize)
  tick.tick1line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick2line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick1line.set_markersize(MP_TICKSIZE)
  tick.tick2line.set_markersize(MP_TICKSIZE)
for tick in P.gca().xaxis.get_minor_ticks():
  tick.tick1line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick2line.set_markeredgewidth(MP_LINEWIDTH)
  tick.tick1line.set_markersize(0.5*MP_TICKSIZE)
  tick.tick2line.set_markersize(0.5*MP_TICKSIZE)

print "Run took", round(time.time() - tstart, 1), "sec."
P.show()

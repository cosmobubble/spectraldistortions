#!/usr/bin/python
"""
Plot delta_dL as a function of redshift.
"""

import numpy as np
import cosmolopy as cp
import pylab as P
from matplotlib import ticker
import scipy.interpolate
import distortions

cosmo = distortions.DEFAULT_COSMO

# TESTING
ells = np.logspace(0.1, np.log10(3000.), 30)
m = distortions.Model(phi0=1e-2, r0=1e3, cosmo=cosmo)


#print 10., distortions.Cl_ksz_nonlin(m, l=10.)
#print 3000., distortions.Cl_ksz_nonlin(m, l=3000.)
#exit()

lin = []; nonlin = []
for _l in ells:
    lin.append(  distortions.Cl_ksz(m, l=_l)  )
    nonlin.append(  distortions.Cl_ksz_nonlin(m, l=_l)  )
    
lin = np.array(lin)
nonlin = np.array(nonlin)

print lin
print nonlin

P.subplot(111)
P.plot(ells, ells*(ells+1)*lin/(2.*np.pi), 'b-', lw=1.4)
P.plot(ells, ells*(ells+1)*nonlin/(2.*np.pi), 'r-', lw=1.4)
#P.xscale('log')
P.show()


exit()

# Define parameter space
r0 = np.logspace(2.5, np.log10(1.2e4), 6)
phi0 = 1e-2

zs = np.logspace(-2., 3., 50.)
ddl = []
for _r0 in r0:
  m = distortions.Model(phi0=phi0, r0=_r0, cosmo=cosmo)
  _dl, _ddl = distortions.dL_perturbed(zs, m)
  ddl.append(_ddl)

P.subplot(111)
for i in range(len(ddl)):
  P.plot(zs, ddl[i], lw=1.4, label="r0="+str(int(r0[i]))+" Mpc")
P.axhline(0., ls='dotted', color='k')
P.xscale('log')
P.xlabel("z", fontdict={'fontsize':'24'}, labelpad=-15.)
P.ylabel("$\delta d_L$", fontdict={'fontsize':'28'})
P.title("$\Phi_0 = 10^{-2}$")
P.legend(loc='lower right', prop={'size':'small'})
P.show()

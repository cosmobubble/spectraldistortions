#!/usr/bin/python
"""
Plot KSZ power spectrum at l=3000, using Zibin & Moss units;
T_0^2 l(l+1) C_l/2pi [uK^2]
"""

import numpy as np
import cosmolopy as cp
import pylab as P
from matplotlib import ticker

phi0 = np.linspace(1e-4, 1e-2, 10)
r0 = np.linspace(1e3, 1e4, 11)

R0, PHI0 = np.meshgrid(r0, phi0)

Z = 1e5 * PHI0 * np.exp(-((R0-2e3)/3e3)**2.)
ZZ = PHI0 * np.array( [1e5 * np.exp(-((r0-2e3)/3e3)**2.), ]*phi0.size )

print np.sum(ZZ - Z)

P.subplot(111)
ctr = P.contour(R0, PHI0, Z, locator=ticker.LogLocator())
P.clabel(ctr, inline=1, fontsize='small', fmt='%1.0f')
#P.clabel(ctr, inline=1, fontsize='small', fmt=ticker.LogFormatterMathtext())
P.show()

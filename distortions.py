#!/usr/bin/python
"""
Calculate spectral distortions and distance-redshift relation 
corrections in a perturbed FLRW Universe.
Implements the full linear perturbative expression for the 
luminosity distance from Bonvin, Durrer and Gasparini
(arXiv:astro-ph/0511183v5).
"""

import numpy as np
import scipy.integrate
import scipy.interpolate
from scipy.special import jn, iv
import cosmolopy as cp
import os

# Define default cosmology
DEFAULT_COSMO = {
 'omega_M_0' : 0.266,
 'omega_lambda_0' : 0.734,
 'omega_k_0' : 0.0,
 'omega_b_0' : 0.045,
 'omega_n_0' : 0.0,
 'omega_r_0' : 0.0001, # FIXME: radiation density
 'N_nu' : 0,
 'Y_He' : 0.24, # Helium fraction
 'h' : 0.71,
 'n' : 0.96,
 'sigma_8' : 0.8,
 'baryonic_effects': False,
 'gamma': 0.55,
 'tau0': 0., # Optical depth due to baryons today
 'z_re': 10. # Redshift of reionisation (instantaneous)
}

Z_LSS = 1090.79 # Redshift of last-scattering surface
N_TAU_SAMPLES = 500 # No. of optical depth history Tau(z) samples
N_Z_SAMPLES = 200 # No. redshift samples down central observer's past lightcone
N_LCONE_SAMPLES = 300 # No. sample coordinate points along lightcones
N_DIPOLE_SAMPLES = 20 # No. angle samples in full dipole calculation (good to within about 1e-3 for 20 samples)
N_INTEGRATED_GROWTH_SAMPLES = 200 # No. samples to use in integration of linear growth factor
N_DL_ETA_SAMPLES = 200 # No. eta samples used for computing perturbed dL(z)
T0 = 2.725 # CMB monopole, in K
C = 3e5 # Speed of light, in km/s

# Directory where non-linear P(k) files are stored
CLASS_DIR = "/home/phil/postgrad/software/class/output/"

########################################################################
# Construct class for keeping track of precomputed quantities
########################################################################

class Model(object):
    """
    Class which contains information about cosmological model, and 
    precomputed quantities.
    """
    def __init__( self, phi0, r0, cosmo=DEFAULT_COSMO,
                  precomp=None, use_full_dipole=False ):
        self.precomp = precomp
        self.use_full_dipole = use_full_dipole
        
        # Set basic cosmological/profile parameters
        self.cosmo = cosmo
        self.r0 = r0
        self.phi0 = phi0
        
        # Thomson scattering coefficients
        self.fb = cosmo['omega_b_0'] / cosmo['omega_M_0'] # Baryon fraction
        self.alpha = cosmo['omega_M_0'] * (2. - cosmo['Y_He'])
        self.alpha *= 1.154e-5 * cosmo['h']**2. * self.fb
        
        # Precompute commonly-used quantities
        self.precompute_evolution()
    
    def precompute_evolution(self):
        """Precompute cosmological quantities that are frequently used."""
        # Load precomputed values from a file, if they exist
        if self.precomp != None:
          self.z, self.r, self.Beta = np.load(self.precomp)
        else:
          self.z = np.concatenate(([0.],np.logspace(-5, np.log10(Z_LSS*1.01), N_Z_SAMPLES)))
          self.r = cp.distance.comoving_distance(self.z, z0=0., **self.cosmo)
        
        # Precompute redshift/conformal time relation
        # NB. eta = r, since I've chosen units of distance for eta
        self.eta = scipy.interpolate.interp1d(self.z, self.r, kind='linear')
        self.z_eta = scipy.interpolate.interp1d(self.r, self.z, kind='linear')
        
        # Precompute linear growth factor down past lightcone
        self.Dz = cp.perturbation.fgrowth(self.z, self.cosmo['omega_M_0'])
        self.D = scipy.interpolate.interp1d(self.z, self.Dz, kind='linear')
        
        # Calculate Beta(z) if it's not already been computed
        if self.precomp == None:
          if self.use_full_dipole:
            self.Beta = np.array( [dipole_full(self, _z) for _z in self.z] )
          else:
            self.Beta = beta(self.z, self)
        self.Beta[np.where(np.isnan(self.Beta))] = 0.
        
        # Precompute differential optical depth (multiply by alpha afterwards)
        okfac = self.cosmo['omega_k_0']*(100.*self.cosmo['h']*self.r/C)**2.
        self.dtaudr =  (1.+self.z)**2. / np.sqrt(1. - okfac)

    def load_nonlinear_pk(self):
        """
        Load pre-computed non-linear P(k) from CLASS output files, 
        and create an interpolator for it. Make sure that the CLASS 
        input parameters match up with cosmological parameters used here!
        """
        # Load generated non-linear P(k) from files
        zsamp = []; pk = []; kk = []
        for item in os.listdir(CLASS_DIR):
          if "pk_nl.dat" in item:
            
            # Get redshift
            f = open(CLASS_DIR+item, 'r')
            zsamp.append(  float(f.readlines()[0].split("z=")[1])  )
            f.close()
            
            # Get P(k) data (all k should be the same)
            _k, _pk = np.genfromtxt(CLASS_DIR+item).T
            kk.append(_k)
            pk.append(_pk)
        
        # Convert to numpy arrays, convert to our units
        zsamp = np.array(zsamp)
        kk = np.array(kk) * self.cosmo['h']
        pk = np.array(pk) / (self.cosmo['h'])**3.
        
		# Sort results in redshift and set max. non-linear redshift
        idxs = np.argsort(zsamp)
        zsamp = zsamp[idxs]; kk = kk[idxs]; pk = pk[idxs]
        self.zmax_nonlin = np.max(zsamp)
        
        # Create interpolating function in (log(k), z) space for log(P(k))
        K, Z = np.meshgrid(np.log10(kk[0]), zsamp)
        coords = np.column_stack( (K.flatten(), Z.flatten()) )
        # (Interp. fill value: small number, 10^-6, because not all P(k) 
        # can be efficiently calculated by CLASS for very large k -- needed 
        # when l>>1 due to Limber approx. in Cl_ksz non-lin. calc.)
        PK = scipy.interpolate.LinearNDInterpolator( coords,
                                                     np.log10(pk.flatten()),
                                                     fill_value=-6. )
        self.logpk_nonlinear = PK
        

########################################################################
# Geodesics and directional quantities
########################################################################

class LightCone(object):
    def __init__( self, m, zo, zs, theta=np.pi/2., phi=0.):
        """
        Calculate (background) null geodesic coordinates along some LOS 
        for a remote observer.
         * zo and zs are observer and source redshifts
         * theta', phi' are directions on an observer's sky
         * nsamples is the number of samples to pre-calculate
        """
        # TODO: Manually add higher density of samples around r=0.
        # This helps with integrals of the form (grad Psi dot n), 
        # where there is a sign change around z=0 (can then reduce nsamples)
        
        # Store input arguments
        self.zs = zs; self.zo = zo
        self.theta = theta; self.phi = phi
        self.nsamples = N_LCONE_SAMPLES
        
        # Observer position
        ro = m.eta(zo); self.ro = ro
        
        # Pre-calculate geodesic points (z is used as time coord.)
        if zo > 1e-5: # better sampling in log space
            self.zt = np.logspace(np.log10(zo), np.log10(zs), self.nsamples)
        else:
            self.zt = np.concatenate( ([0.], np.logspace(-5., 
                                       np.log10(zs), self.nsamples)) )
        self.eta = m.eta(self.zt)
        
        # Take difference of interpolated eta (direct comp. is expensive)
        #rr = cp.distance.comoving_distance(self.zt, z0=zo, **m.cosmo)
        rr = m.eta(self.zt) - m.eta(zo)
        
        # Comoving distances along inbound/outbound legs
        # FIXME: Obsolete
        self.r_in = ro - rr
        self.r_out = ro + rr
        
        # Calculate dot product of LOS vector n with radial unit 
        # vector along the entire LOS. Useful for (n . grad Psi).
        # Also calculates trajectory of LOS in global spherical 
        # polar coord. system, (r, theta, phi)
        ndot, _r, _th, _ph = los_dot_product(rr, ro, theta, phi)
        self.n_dot_rhat = ndot
        self.global_r = _r
        self.global_theta = _th
        self.global_phi = _ph
        
def los_dot_product(rlos, ro=0., theta=np.pi/2., phi=0.):
    """
    Scalar product (n . r_hat) in a spherically-symmetric field. 
    n is the unit vector line-of-sight direction starting at 
    radius ro, in direction (theta', phi') on the sky of the 
    observer at ro. r_hat is the radial direction in the global 
    sph. polar coord. system. Take the dot product down a LOS 
    with comoving separations from the observer given by rlos(=eta).
    
    Note that (theta', phi') aren't the (theta, phi) associated 
    with the global spherical polar coord. system; they are just 
    defined on the sky of the observer at ro.
    """
    # rlos is a line in comoving space of some length
    # ro is the radius of some observer from the centre of symmetry
    # theta, phi are direction of the source on the *observer's* sky
    xo = ro; yo = 0.; zo = 0.
    
    # Position in global Cartesian coord. system
    _x = xo + rlos*np.sin(theta)*np.cos(phi)
    _y = yo + rlos*np.sin(theta)*np.sin(phi)
    _z = zo + rlos*np.cos(theta)
    
    # Angle in global sph. polar coord. system
    np.seterr(invalid='ignore')
    th = np.arccos(_z/rlos); th[np.isnan(th)] = np.pi/2.
    ph = np.arctan(_y/_x); ph[np.isnan(ph)] = 0.
    np.seterr(invalid='warn')
    ph[np.where(ph < 0.)] = np.pi + ph[np.where(ph < 0.)]
    r = np.sqrt(_x**2. + _y**2. + _z**2.)
    
    # Dot product
    dp =   np.sin(th)*np.cos(ph) * np.sin(theta)*np.cos(phi) \
         + np.sin(th)*np.sin(ph) * np.sin(theta)*np.sin(phi) \
         + np.cos(th) * np.cos(theta)
    return dp, r, th, ph


########################################################################
# Background and perturbative quantities
########################################################################

def omegaM(z, m):
	"""Evolving matter fraction, Omega_M(a), from Linder (nr. Eq. 5),
	   arXiv:astro-ph/0507263 (2005)."""
	a = 1. / (1. + z)
	return m.cosmo['omega_M_0'] * a**(-3.) / cp.distance.e_z(z, **m.cosmo)**2.

def fg(z, m):
    """Linear growth rate, parameterised as in Linder."""
    if 'gamma' in m.cosmo.keys():
      gamma = m.cosmo['gamma']
    else:
      gamma = 0.55 # Default LCDM/GR value
    return omegaM(z, m)**gamma

def E(z, m):
    """
    Dimensionless Hubble rate as fn. of redshift, including 
    radiation term.
    """
    x = 1.+z; p = m.cosmo
    return np.sqrt( p['omega_r_0']*x**4. + p['omega_M_0']*x**3. \
                   + p['omega_k_0']*x**2. + p['omega_lambda_0'] )

def eta_direct(z, m):
    """
    Conformal time at some background redshift z. Use the relation 
    between comoving distance and conformal time: eta = chi / c
    """
    # FIXME: Need to check units (here, set c=1)
    return cp.distance.comoving_distance(z, z0=0., **m.cosmo)

def integrated_growth_factor(z, m):
    """
    Integral of the linear growth factor D(a) with respect to time, 
    which gives the time-evolution of a linear velocity perturbation.
    (See Eq. 4.82 of Dodelson, Modern Cosmology, for diff. eqn.)
    """
    # Get scale factor samples
    amax = 1. / (1. + z)
    amin = 1. / (1. + np.max(m.z)) # FIXME: Will this introduce an integration error?
    a = np.logspace(np.log10(amin), np.log10(amax), N_INTEGRATED_GROWTH_SAMPLES)
    zz = (1./a) - 1.
    
    # Construct integrand samples, then integrate
    D = m.D(zz)
    H = 100. * m.cosmo['h'] * E(zz, m) # km/s/Mpc
    integ = D / (a*H)
    I = scipy.integrate.simps(integ, a)
    return C**2. * I # Units of Mpc

########################################################################
# Quantities derived from the Newtonian potential
########################################################################

def delta(r, zt, m):
    """Density fluctuation from the Poisson equation."""
    # del^2 Psi = Psi'' + 2/r Psi' = 4 pi G <rho> a^2 delta
    # Units of Mpc^-2 already folded into density expression
    delta = psi_laplacian(r, zt, m)
    delta /= 1.5 * m.cosmo['omega_M_0'] * (1.+zt) * (100.*m.cosmo['h']/C)**2.
    return delta

@np.vectorize
def velocity(r, zt, m):
    """
    Peculiar velocity due to potential, from momentum conservation 
    equation (Eq. 4.82 of Dodelson). Units of km/s.
    """
    # (Need D to cancel factor of D in psi_grad)
    fac = integrated_growth_factor(zt, m) # Units are km s^-1 Mpc
    D = m.D(zt)
    grad = psi_grad(r, zt, m) # Units are Mpc^-1
    return -1. * grad * fac / D

def psi_fourier(k, zt, m):
    """
    Potential Psi in Fourier (Bessel-Legendre) space, calculated 
    analytically for Gaussian profile. The transform convention used is 
    that of Leistedt et al., A&A 540, A60 (2012), Eq. 2. Calculated for 
    l=0 (monopole), since all other modes are zero due to sph. sym.
    NB: This expression may differ from convention used by Zibin & Moss!
    """
    r0 = m.r0
    x = (k*r0)**2. / 8.
    D = m.D(zt)
    psik = (4. - (k*r0)**2.) * iv(0, x) + (k*r0)**2. * iv(1, x)
    psik *= 4. * np.pi * k * r0**3. * np.exp(-x) / np.sqrt(128.)
    psik *= D * m.phi0
    return psik

########################################################################
# Definition of the Newtonian potential, and derivatives of it
########################################################################

def psi(r, zt, m):
    """
    Newtonian potential Psi(r, zt), where zt is background redshift 
    as time coord.
    """
    fr = np.exp(-(np.abs(r)/m.r0)**2.)
    D = m.D(zt)
    return D * m.phi0 * fr

def psi_grad(r, zt, m):
    """Gradient of Psi(eta), which can be calculated analytically."""
    # Assumes spherical symmetry; should be vector quantity
    r0 = m.r0
    fr = -2.*np.abs(r)/r0**2. * np.exp(-(np.abs(r)/r0)**2.)
    D = m.D(zt)
    return D * m.phi0 * fr

def psi_laplacian(r, zt, m):
    """Laplacian of Psi(eta), which can be calculated analytically."""
    r0 = m.r0
    fr = (2./r0**2.) * np.exp(-(np.abs(r)/r0)**2.) * (3. - 2.*(r/r0)**2.)
    D = m.D(zt)
    return D * m.phi0 * fr

def psi_dot(r, zt, m):
    """
    Conformal time derivative of Psi(eta). (See p5 of Bonvin et al., 
    near Eq. 39, to see that the overdot means conformal time derivative.)
    """
    fac = fg(zt, m) * (100.*m.cosmo['h']) * E(zt, m) / ((1. + zt) * C)
    return fac * psi(r, zt, m)

def psi_ddot(r, zt, m):
    """Conformal time second derivative of Psi(eta)."""
    g = m.cosmo['gamma']
    f = fg(zt, m)
    EE = E(zt, m)
    D = m.D(zt)
    a = 1. / (1. + zt)
    Dddot = f*EE**2. - g*m.cosmo['omega_k_0']*a**-2.
    Dddot += (3.*g - 1.)*m.cosmo['omega_lambda_0'] + m.cosmo['omega_M_0']*a**-3.
    Dddot *= (100.*m.cosmo['h']/C)**2. * a**2. * f # * D
    return Dddot * psi(r, zt, m)
    

########################################################################
# Distance/redshift integrals
########################################################################

@np.vectorize
def dL_perturbed(zs, m):
    """
    Perturbed luminosity distance dL(z_s), from Eq. 59 of Bonvin et al., 
    where z_s is the *observed* (i.e. perturbed) redshift of the source.
    """
    # Note that Phi = Psi, using metric convention from Eq. 36 of Bonvin.
    
    # FIXME: Missing peculiar velocity terms. Shouldn't these be added 
    # in, since the potential will cause a peculiar velocity? (Only matters 
    # off-centre)
    # FIXME: Need to check signs, since (eta_s - eta_o) sign has flipped 
    # in my convention
    
    # Source/observer redshift, conformal time, and peculiar potential
    zo = 0.
    eta_o = m.eta(zo)
    eta_s = m.eta(zs)
    psi_o = psi(eta_o, zo, m)
    psi_s = psi(eta_s, zs, m)
    
    # Background dL(z) term
    dL = (1. + zs) * (eta_o - eta_s)
    
    # Sample eta and find corresponding z
    n = np.linspace(eta_o, eta_s, N_DL_ETA_SAMPLES) # FIXME: No. of samples required?
    zz = m.z_eta(n)
    
    # Gravitational redshift terms (line 2 of Eq. 59)
    # (See also 2012 erratum to this article for modification to these terms)
    # {H}=H*a, nr. Eq.55 in Bonvin (km/s/Mpc)
    _Hs = (100.*m.cosmo['h']) * E(zs, m) / (1. + zs)
    zgrav1 = -1. * (psi_s - psi_o) * C / (_Hs*(eta_o - eta_s))
    zgrav2 = -psi_s
    
    # Get potentials and derivatives
    _psi = psi(n, zz, m)
    _psidot = psi_dot(n, zz, m)
    _psiddot = psi_ddot(n, zz, m)
    _psilaplace = psi_laplacian(n, zz, m)
    
    # Integrated LOS terms (line 3 of Eq. 59)
    integ3 = (n - eta_s) * _psidot
    integ4 = (n - eta_s) * (eta_o - n) * _psiddot
    y1 = scipy.integrate.simps(_psi, n)
    y2 = scipy.integrate.simps(_psidot, n)
    y3 = scipy.integrate.simps(integ3, n)
    y4 = scipy.integrate.simps(integ4, n)
    
    los1 = 2. * y1 / (eta_o - eta_s)
    los2 = 2. * y2 * C / ( (eta_o - eta_s) * _Hs )
    los3 = 2. * y3 / (eta_o - eta_s)
    los4 = y4 / (eta_o - eta_s)
    
    # Lensing term
    integ5 = ((n-eta_s) * (eta_o-n) / (eta_o-eta_s)) * _psilaplace
    y5 = scipy.integrate.simps(integ5, n)
    lens = -1. * y5
    
    # Return final result
    delta = zgrav1 + zgrav2 + los1 + los2 + los3 + los4 + lens
    return dL, delta, [zgrav1, zgrav2, los1, los2, los3, los4, lens]

@np.vectorize
def dA_perturbed(zs, m):
    """
    Perturbed angular diameter distance dA(z_s), using Eqs. 57 & 59 of 
    Bonvin et al. to give the perturbed redshift/luminosity distance, 
    and then using an expansion to linear order of the reciprocity 
    theorem to get the perturbation to dA(z).
    """
    dL, deltaL, comp = dL_perturbed(zs, m)
    deltaz = delta_z(m, 0., Z_LSS)
    return deltaL - 2.*deltaz/(1.+Z_LSS)

#@np.vectorize
def delta_z(m, zo, zs, theta=np.pi/2., phi=0.):
    """
    Perturbed source redshift z_s, from Eq. 57 of Bonvin et al.
    ro is the radius of the observer from the centre of symmetry,
    and (theta, phi) are the direction of the source on the 
    observer's sky.
    """
    # FIXME: There may be a sign error here, since Bonvin et al. 
    # define (-n) to be the direction of the source from the observer.
    # I may have assumed the opposite in the n_dot_rhat calculation, 
    # so a minus sign may be missing anywhere (n . vector) terms appear.
    # This doesn't affect the KSZ Cl's/y-distortion, since they depend 
    # on Beta^2 only.
    sgn = +1.
    
    # Solve null geodesic eqn. on lightcone
    l = LightCone(zo=zo, zs=zs, theta=theta, phi=phi, m=m)
    
    # Get potentials at off-centre observer and source
    psi_o = psi(l.global_r[0],  l.zt[0], m)
    psi_s = psi(l.global_r[-1], l.zt[-1], m)
    
    # Get LOS velocity component for observer
    v = velocity(l.global_r[0], zo, m) / C
    nhat = sgn * los_dot_product(np.zeros(1), l.global_r[0], theta, phi)[0][0]
    
    # Integral of potential gradient from discrete samples
    _psigrad = sgn * l.n_dot_rhat * psi_grad(l.global_r, l.zt, m)
    y = scipy.integrate.simps(_psigrad, l.eta)
    
    # Return result
    return ( (1. + zs) / (1. + zo) ) * ( psi_s - psi_o - 2.*y + v*nhat )

@np.vectorize
def beta(zt, m):
    """
    Calculate an approximation to the dipole as seen by an observer 
    at a given point down our past lightcone. Uses Eq. 57 of Bonvin.
    """
    # Solve null geodesic eqn. on lightcone (+/- r directions)
    l = LightCone(zo=zt, zs=Z_LSS, m=m)
    
    # Get potentials at off-centre observer, and source in in/outbound directions
    psi_o = psi(l.r_in[0], l.zt[0], m)
    psi_s_in = psi(l.r_in[-1], l.zt[-1], m)
    psi_s_out = psi(l.r_out[-1], l.zt[-1], m)
    
    # Integral of potential gradient (inbound and outbound) from discrete samples
    psi_grad_in = -1. * np.sign(l.r_in) * psi_grad(l.r_in, l.zt, m)
    psi_grad_out = psi_grad(l.r_out, l.zt, m)
    y_in = scipy.integrate.simps(psi_grad_in, l.eta)
    y_out = scipy.integrate.simps(psi_grad_out, l.eta)
    
    # Get LOS velocity component for observer
    v = velocity(l.global_r[0], zt, m) / C
    
    # Get beta = delta_z / (1+z_in + 1 + z_out) in each direction (for z_in ~ z_out).
    beta = (psi_s_out - psi_s_in) - 2.*(y_in - y_out) - 2.*v
    return 0.5 * beta
    

########################################################################
# Ionisation/optical depth history
########################################################################

def tau_history(m):
    """
    Model for the optical depth as a function of redshift, from Eq. 18 
    of Zibin & Moss, arXiv:1105.0909.
    """
    # Integrate to find Tau(z), with instantaneous reionisation approx.
    dtaudz = lambda _x, zz: (zz < m.cosmo['z_re']) * (1. + zz)**2. \
                             / cp.distance.e_z(zz, **m.cosmo)
    z = np.concatenate( ([0.], np.logspace(-5, np.log10(Z_LSS), N_TAU_SAMPLES)) )
    tau = m.alpha * scipy.integrate.odeint(dtaudz, m.cosmo['tau0'], z)
    return tau, z

def dtau_dz(z, m):
    """Optical depth derivative, from Zibin & Moss Eq. 18."""
    # Return dTau/dz with instantaneous reionisation approx.
    return m.alpha * (z < m.cosmo['z_re']) * (1. + z)**2. \
                             / cp.distance.e_z(z, **m.cosmo)


########################################################################
# SZ/y-distortion integrals
########################################################################

def kineticSZ(m):
    """
    Integrated Kinetic SZ DeltaT/T, from Zibin & Moss Eq. 5 (potential 
    self-interaction term).
    """
    # FIXME: Outdated.
    
    # Redshift range for integration (out to reionisation)
    z = np.linspace(0., m.cosmo['z_re'], 80)
    r = cp.distance.comoving_distance(z, z0=0., **m.cosmo)
    
    # Sample fns in integrand for range of redshift
    dtdz = dtau_dz(z, m)
    _beta = np.array([beta(zz, m) for zz in z])
    _delta = delta(r, z, m)
    
    # Integrate samples
    dT = scipy.integrate.simps(dtdz * _beta * _delta, z)
    return dT

def Cl_ksz(m, l=3000., use_harmonic_integral=False):
    """
    Compute angular power spectrum coefficient for kinetic SZ, using 
    the Limber approximation, as set out in Eq. 29 of Zibin & Moss.
    """
    
    # Define k-range for integration, using the Limber approximation
    k = 0.5*(2.*l + 1.) / m.r # k [Mpc^-1], r [Mpc]. FIXME: Check if h^-1 Mpc or not
    k[np.where(np.isinf(k))] = 1e7 # Remove inf, replace with large number
    
    # Matter power spectrum down past light-cone
    Pk0 = cp.perturbation.power_spectrum(k, 0.0, **m.cosmo)
    Pk = Pk0 * m.Dz**2.
    
    # Perform integration, using either real-space or harmonic-space integral
    if use_harmonic_integral:
        integ = (m.dtaudr * m.Beta)**2. * Pk
        Cl = scipy.integrate.simps(integ, k)
        Cl *= -m.alpha**2. * 16.*np.pi**3. / (2.*l + 1.)
    else:
        integ = (m.dtaudr * m.Beta)**2. * Pk / m.r**2.
        integ[0] = 0. # Fix initial value to deal with r^-2 -> 1/0
        Cl = scipy.integrate.simps(integ, m.r)
        Cl *= m.alpha**2. * 8.*np.pi**3.
    return Cl

def Cl_ksz_nonlin(m, l=3000., use_harmonic_integral=False):
    """
    Compute angular power spectrum coefficient for kinetic SZ, using 
    the Limber approximation, with a non-linear matter power spectrum.
    """
    
    # Define k-range for integration, using the Limber approximation
    k = 0.5*(2.*l + 1.) / m.r # k [Mpc^-1], r [Mpc].
    k[np.where(np.isinf(k))] = 1e7 # Remove inf, replace with large number
    
    # Non-linear matter power spectrum down past light-cone
    m.load_nonlinear_pk() # Load external non-lin. P(k) data
    
    # Segregate (k,z) values depending on whether linear/non-linear z
    il = np.where(m.z > m.zmax_nonlin, True, False)
    inl = np.where(m.z > m.zmax_nonlin, False, True)
    
    # Calculate P(k) in each regime
    pk_nl = 10.**m.logpk_nonlinear(np.log10(k[inl]), m.z[inl])
    pk_lin = m.Dz[il]**2. * cp.perturbation.power_spectrum(k[il], 0.0, **m.cosmo)
    Pk = np.concatenate((pk_nl, pk_lin))
    
    # Perform integration, using either real-space or harmonic-space integral
    if use_harmonic_integral:
        integ = (m.dtaudr * m.Beta)**2. * Pk
        Cl = scipy.integrate.simps(integ, k)
        Cl *= -m.alpha**2. * 16.*np.pi**3. / (2.*l + 1.)
    else:
        integ = (m.dtaudr * m.Beta)**2. * Pk / m.r**2.
        integ[0] = 0. # Fix initial value to deal with r^-2 -> 1/0
        Cl = scipy.integrate.simps(integ, m.r)
        Cl *= m.alpha**2. * 8.*np.pi**3.
    return Cl #, k, Pk

def ydistortion(m):
    """
    Calculate the Compton y-distortion parameter, defined in Eq. 39 of 
    Zibin & Moss.
    """
    integ = m.dtaudr * m.Beta**2.
    y = scipy.integrate.simps(integ, m.r)
    return (7./10.) * m.alpha * y

def dydr(m):
    """Derivative of the Compton y-distortion parameter."""
    return (7./10.) * m.alpha * m.dtaudr * m.Beta**2.

def dipole_full(m, zo, zs=Z_LSS):
    """
    Calculate dipole in redshift distribution from the full angular 
    distribution of delta_z, with a spherical harmonic transformation.
    """
    # Theta (angle) range
    th = np.linspace(0., np.pi, N_DIPOLE_SAMPLES)
    
    # Get delta_z distribution as fn. of angle
    dz = [delta_z(m, zo, zs, theta=np.pi/2., phi=_th) for _th in th]
    dz = np.array(dz) * (1. + zo) / (1. + zs) # FIXME: Is this right?
    
    # Integrate with Y(1,0) kernel to find a_10 sph. harmonic coeff.
    integ = np.sin(th) * np.cos(th) * dz
    a_10 = np.sqrt(np.pi) * scipy.integrate.simps(integ)
    beta = -0.5 * np.sqrt(1. / (4. * np.pi)) * a_10
    return beta



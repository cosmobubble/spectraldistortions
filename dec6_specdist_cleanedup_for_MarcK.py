#!/usr/bin/python
"""
   Implements the full linear perturbative expression for the 
   luminosity distance from Bonvin, Durrer and Gasparini 
   (arXiv:astro-ph/0511183v5), and uses this to calculate 
   spectral distortions due to large spherical potential.
     -- Phil Bull (phil.bull@astro.ox.ac.uk), Dec 2012
"""
import numpy as np
import scipy.integrate
import scipy.interpolate
from scipy.special import jn
import cosmolopy as cp
import pylab as P

# Define default cosmology
DEFAULT_COSMO = {
 'omega_M_0' : 0.266,
 'omega_lambda_0' : 0.734,
 'omega_k_0' : 0.0,
 'omega_b_0' : 0.045,
 'omega_n_0' : 0.0,
 'omega_r_0' : 0.001, # FIXME: radiation density
 'N_nu' : 0,
 'Y_He' : 0.24, # Helium fraction
 'h' : 0.71,
 'n' : 0.96,
 'sigma_8' : 0.8,
 'baryonic_effects': False,
 'gamma': 0.55,
 'r0': 2e3,   # Length scale of potential
 'phi0': 1e0, # Potential amplitude
 'tau0': 0., # Optical depth due to baryons today
 'z_re': 10. # Redshift of reionisation (instantaneous)
}

Z_LSS = 1090. # Redshift of last-scattering surface
N_TAU_SAMPLES = 500 # No. of optical depth history Tau(z) samples
N_LCONE_SAMPLES = 300 # No. sample coordinate points along lightcones
T0 = 2.725 # CMB monopole, in K
C = 3e5 # Speed of light, in km/s

########################################################################
# Geodesics
########################################################################

class LightCone(object):
    def __init__(self, zo, zs, nsamples=N_LCONE_SAMPLES, cosmo=DEFAULT_COSMO):
        """
        Calculate (background) null geodesic coordinates for the whole
        lightcone of a remote observer.
         * zo and zs are observer and source redshifts
         * nsamples is the number of samples to pre-calculate
        """
        # TODO: Manually add higher density of samples around r=0.
        # This helps with integrals of the form (grad Psi dot n), 
        # where there is a sign change around z=0 (can then reduce nsamples)
        
        # Store input arguments
        self.zs = zs; self.zo = zo
        self.nsamples = nsamples
        
        # Observer position
        ro = eta(zo); self.ro = ro
        
        # Pre-calculate geodesic points (z is used as time coord.)
        if zo > 1e-5: # better sampling in log space
            self.zt = np.logspace(np.log10(zo), np.log10(zs), nsamples)
        else:
            self.zt = np.concatenate( ([0.], np.logspace(-5., 
                                       np.log10(zs), nsamples)) )
        self.eta = eta(self.zt)
        rr = cp.distance.comoving_distance(self.zt, z0=zo, **cosmo)
        
        # Comoving distances along inbound/outbound legs
        self.r_in = ro - rr
        self.r_out = ro + rr
        

########################################################################
# Background and perturbative quantities
########################################################################

def omegaM(z, cosmo=DEFAULT_COSMO):
	"""Evolving matter fraction, Omega_M(a), from Linder (nr. Eq. 5),
	   arXiv:astro-ph/0507263 (2005)."""
	a = 1. / (1. + z)
	return cosmo['omega_M_0'] * a**(-3.) / cp.distance.e_z(z, **cosmo)**2.

def fg(z, cosmo=DEFAULT_COSMO):
	"""Linear growth rate, parameterised as in Linder."""
	if 'gamma' in cosmo.keys():
		gamma = cosmo['gamma']
	return omegaM(z, cosmo)**gamma

def E(z, cosmo=DEFAULT_COSMO):
    """
    Dimensionless Hubble rate as fn. of redshift, including 
    radiation term.
    """
    x = 1.+z; p = cosmo
    return np.sqrt( p['omega_r_0']*x**4. + p['omega_M_0']*x**3. \
                   + p['omega_k_0']*x**2. + p['omega_lambda_0'] )

def eta_direct(z, cosmo=DEFAULT_COSMO):
    """
    Conformal time at some background redshift z. Use the relation 
    between comoving distance and conformal time: eta = chi / c
    """
    # FIXME: Need to check units (here, set c=1)
    return cp.distance.comoving_distance(z, z0=0., **cosmo)

def integrated_growth_factor(z, cosmo=DEFAULT_COSMO):
    """
    Integral of the linear growth factor D(a) witb respect to time, 
    which gives the time-evolution of a linear velocity perturbation.
    (See Eq. 4.82 of Dodelson, Modern Cosmology, for diff. eqn.)
    """
    # Get scale factor samples
    amax = 1. / (1. + z)
    a = np.logspace(-7., np.log10(amax), 200)
    zz = (1./a) - 1.
    
    # Construct integrand samples, then integrate
    D = cp.perturbation.fgrowth(zz, cosmo['omega_M_0'])
    H = 100. * cosmo['h'] * E(zz, cosmo) # km/s/Mpc
    integ = D / (a*H)
    I = scipy.integrate.simps(integ, a)
    return C**2. * I # Units of Mpc

########################################################################
# Definition of the Newtonian potential, and derivatives of it
########################################################################

def delta(r, zt, cosmo=DEFAULT_COSMO):
    """Density fluctuation from the Poisson equation."""
    # del^2 Psi = Psi'' + 2/r Psi' = 4 pi G <rho> a^2 delta
    # Units of Mpc^-2 already folded into density expression
    delta = psi_laplacian(r, zt, cosmo)
    delta /= 1.5 * cosmo['omega_M_0'] * (1.+zt) * (100.*cosmo['h']/C)**2.
    return delta

@np.vectorize
def velocity(r, zt, cosmo=DEFAULT_COSMO):
    """
    Peculiar velocity due to potential, from momentum conservation 
    equation (Eq. 4.82 of Dodelson). Units of km/s.
    """
    # (Need D to cancel factor of D in psi_grad)
    fac = integrated_growth_factor(zt, cosmo) # Units are km s^-1 Mpc
    D = cp.perturbation.fgrowth(zt, cosmo['omega_M_0'])
    grad = psi_grad(r, zt, cosmo) # Units are Mpc^-1
    return -1. * grad * fac

def psi(r, zt, cosmo=DEFAULT_COSMO):
    """
    Newtonian potential Psi(r, zt), where zt is background redshift 
    as time coord.
    """
    fr = np.exp(-(np.abs(r)/cosmo['r0'])**2.)
    D = cp.perturbation.fgrowth(zt, cosmo['omega_M_0'])
    return D * cosmo['phi0'] * fr

def psi_grad(r, zt, cosmo=DEFAULT_COSMO):
    """Gradient of Psi(eta), which can be calculated analytically."""
    # FIXME: Assumes spherical symmetry for now; should be vector quantity
    r0 = cosmo['r0']
    fr = -2.*np.abs(r)/r0**2. * np.exp(-(np.abs(r)/r0)**2.)
    D = cp.perturbation.fgrowth(zt, cosmo['omega_M_0'])
    return D * cosmo['phi0'] * fr

def psi_laplacian(r, zt, cosmo=DEFAULT_COSMO):
    """Laplacian of Psi(eta), which can be calculated analytically."""
    r0 = cosmo['r0']
    fr = (2./r0**2.) * np.exp(-(np.abs(r)/r0)**2.) * (3. - 2.*(r/r0)**2.)
    D = cp.perturbation.fgrowth(zt, cosmo['omega_M_0'])
    return D * cosmo['phi0'] * fr

def psi_dot(r, zt, cosmo=DEFAULT_COSMO):
    """
    Conformal time derivative of Psi(eta). (See p5 of Bonvin et al., 
    near Eq. 39, to see that the overdot means conformal time derivative.)
    """
    fac = fg(zt, cosmo) * (100.*cosmo['h']) * E(zt, cosmo) / ((1. + zt) * C)
    return fac * psi(r, zt, cosmo)

def psi_ddot(r, zt, cosmo=DEFAULT_COSMO):
    """Conformal time second derivative of Psi(eta)."""
    g = cosmo['gamma']
    f = fg(zt, cosmo)
    EE = E(zt, cosmo)
    D = cp.perturbation.fgrowth(zt, cosmo['omega_M_0'])
    a = 1. / (1. + zt)
    Dddot = f*EE**2. - g*cosmo['omega_k_0']*a**-2.
    Dddot += (3.*g - 1.)*cosmo['omega_lambda_0'] + cosmo['omega_M_0']*a**-3.
    Dddot *= (100.*cosmo['h']/C)**2. * a**2. * f # * D
    return Dddot * psi(r, zt, cosmo)
    

########################################################################
# Distance/redshift integrals
########################################################################

@np.vectorize
def dL_perturbed(zs, cosmo=DEFAULT_COSMO):
    """
    Perturbed luminosity distance dL(eta_s), from Eq. 59 of Bonvin et al., 
    where eta_s is the conformal time at the source.
    """
    # Note that Phi = Psi, using metric convention from Eq. 36 of Bonvin.
    
    # FIXME: Missing peculiar velocity terms. Shouldn't these be added 
    # in, since the potential will cause a peculiar velocity? (Only matters 
    # off-centre)
    # FIXME: Need to check signs, since (eta_s - eta_o) sign has flipped 
    # in my convention
    
    # Source/observer redshift, conformal time, and peculiar potential
    zo = 0.
    eta_o = eta(zo)
    eta_s = eta(zs)
    psi_o = psi(eta_o, zo)
    psi_s = psi(eta_s, zs)
    
    # Background dL(z) term
    dL = (1. + zs) * (eta_o - eta_s)
    
    # Sample eta and find corresponding z
    n = np.linspace(eta_o, eta_s, 200)
    zz = z_eta(n)
    
    # Gravitational redshift terms (line 2 of Eq. 59)
    # (See also 2012 erratum to this article for modification to these terms)
    _Hs = (100.*cosmo['h']) * E(zs) / (1. + zs) # {H}=H*a, nr. Eq.55 in Bonvin (km/s/Mpc)
    zgrav1 = (psi_s - psi_o) * C / (_Hs*(eta_o - eta_s))
    zgrav2 = -psi_s
    
    # Get potentials and derivatives
    _psi = psi(n, zz, cosmo)
    _psidot = psi_dot(n, zz, cosmo)
    _psiddot = psi_ddot(n, zz, cosmo)
    _psilaplace = psi_laplacian(n, zz, cosmo)
    
    # Integrated LOS terms (line 3 of Eq. 59)
    integ3 = (n - eta_s) * _psidot
    integ4 = (n - eta_s) * (eta_o - n) * _psiddot
    y1 = scipy.integrate.simps(_psi, n)
    y2 = scipy.integrate.simps(_psidot, n)
    y3 = scipy.integrate.simps(integ3, n)
    y4 = scipy.integrate.simps(integ4, n)
    
    los1 = 2. * y1 / (eta_o - eta_s)
    los2 = 2. * y2 * C / ( (eta_o - eta_s) * _Hs )
    los3 = -2. * y3 / (eta_o - eta_s)
    los4 = y4 / (eta_o-eta_s)
    
    # Lensing term
    integ5 = ((n-eta_s) * (eta_o-n) / (eta_o-eta_s)) * _psilaplace
    y5 = scipy.integrate.simps(integ5, n)
    lens = -1. * y5
    
    # Return final result
    delta = zgrav1 + zgrav2 + los1 + los2 + los3 + los4 + lens
    return dL, delta

def delta_z(eta_o, eta_s, cosmo=DEFAULT_COSMO):
    """Perturbed z_s, from Eq. 57 of Bonvin et al."""
    # FIXME: Missing peculiar velocity term again
    # FIXME: Doesn't work yet!
    
    # Source/observer redshift, conformal time, and peculiar potential
    zo = z_eta(eta_o); zs = z_eta(eta_s)
    psi_o = psi(eta_o); psi_s = psi(eta_s)
    
    # Integral of potential gradient
    y1 = [scipy.integrate.quad(psi_grad, eta_o, ee, args=(cosmo,))[0] for ee in eta_s]
    y1 = np.array(y1)
    
    # Take dot product with LOS (assumed parallel to radial direction)
    sgn = +1. # or -1.
    
    # Return result
    delta_z = (1. + zs) * ( psi_o - psi_s + 2.*sgn*y1 )
    return delta_z

@np.vectorize
def beta(z, cosmo=DEFAULT_COSMO):
    """
       Calculate an approximation to the dipole as seen by an observer 
       at a given point down our past lightcone. Uses Eq. 57 of Bonvin.
    """
    # Solve null geodesic eqn. on lightcone (+/- r directions)
    l = LightCone(zo=z, zs=Z_LSS, cosmo=cosmo)
    
    # Get potentials at off-centre observer, and source in in/outbound directions
    psi_o = psi(l.r_in[0], l.zt[0])
    psi_s_in = psi(l.r_in[-1], l.zt[-1])
    psi_s_out = psi(l.r_out[-1], l.zt[-1])
    
    # Integral of potential gradient (inbound and outbound) from discrete samples
    psi_grad_in = -1. * np.sign(l.r_in) * psi_grad(l.r_in, l.zt, cosmo=cosmo)
    psi_grad_out = psi_grad(l.r_out, l.zt, cosmo=cosmo)
    y_in = scipy.integrate.simps(psi_grad_in, l.eta)
    y_out = scipy.integrate.simps(psi_grad_out, l.eta)
    
    # Get beta = delta_z / (1+z_in + 1 + z_out) in each direction (for z_in ~ z_out).
    beta = (psi_s_out - psi_s_in) - 2.* (y_in - y_out)
    return beta # psi_s_out, psi_s_in, y_out, y_in
    

########################################################################
# Ionisation/optical depth history
########################################################################

def tau_history(cosmo=DEFAULT_COSMO):
    """
    Model for the optical depth as a function of redshift, from Eq. 18 
    of Zibin & Moss, arXiv:1105.0909.
    """
    # Scaling constants
    fb = cosmo['omega_b_0'] / cosmo['omega_M_0'] # Baryon fraction
    YHe = cosmo['Y_He']; h = cosmo['h']; om = cosmo['omega_M_0']
    alpha = 3.45e-2 * h * fb * om * (2. - YHe) # Pre-factor
    
    # Integrate to find Tau(z), with instantaneous reionisation approx.
    dtaudz = lambda _x, zz: (zz < cosmo['z_re']) * (1. + zz)**2. \
                             / cp.distance.e_z(zz, **cosmo)
    z = np.concatenate( ([0.], np.logspace(-5, np.log10(Z_LSS), N_TAU_SAMPLES)) )
    tau = alpha * scipy.integrate.odeint(dtaudz, cosmo['tau0'], z)
    return tau, z

def dtau_dz(z, cosmo=DEFAULT_COSMO):
    """Optical depth derivative, from Zibin & Moss Eq. 18."""
    # Scaling constants
    fb = cosmo['omega_b_0'] / cosmo['omega_M_0'] # Baryon fraction
    YHe = cosmo['Y_He']; h = cosmo['h']; om = cosmo['omega_M_0']
    alpha = 3.45e-2 * h * fb * om * (2. - YHe) # Pre-factor
    
    # Return dTau/dz with instantaneous reionisation approx.
    return alpha * (z < cosmo['z_re']) * (1. + z)**2. \
                             / cp.distance.e_z(z, **cosmo)


########################################################################
# SZ/y-distortion integrals
########################################################################

def kineticSZ(cosmo=DEFAULT_COSMO):
    """Kinetic SZ DeltaT/T, from Zibin & Moss Eq. 5."""
    
    # Redshift range for integration (out to reionisation)
    z = np.linspace(0., cosmo['z_re'], 80)
    r = cp.distance.comoving_distance(z, z0=0., **cosmo)
    
    # Sample fns in integrand for range of redshift
    dtdz = dtau_dz(z, cosmo)
    _beta = np.array([beta(zz, cosmo) for zz in z])
    _delta = delta(r, z, cosmo)
    
    # Integrate samples
    dT = scipy.integrate.simps(dtdz * _beta * _delta, z)
    return dT

@np.vectorize
def Cl_ksz(  l=3000., cosmo=DEFAULT_COSMO,
             use_precomputed=True, use_harmonic_integral=False ):
    """
    Compute angular power spectrum coefficient for kinetic SZ, using 
    the Limber approximation, as set out in Eq. 29 of Zibin & Moss.
    """
    # Redshift range for integration (out to reionisation)
    #if not use_precomputed:
    #    z = np.linspace(0., cosmo['z_re'], 50) # FIXME: Accuracy?
    #    r = cp.distance.comoving_distance(z, z0=0., **cosmo)
    
    # Scaling constants for optical depth
    fb = cosmo['omega_b_0'] / cosmo['omega_M_0'] # Baryon fraction
    YHe = cosmo['Y_He']; h = cosmo['h']
    om = cosmo['omega_M_0']; ok = cosmo['omega_k_0']
    alpha = 1.15e-5 * fb * om * h**2. * (2. - YHe) # Pre-factor of dTau/dr, in Mpc^-1
    
    # Define k-range for integration, using the Limber approximation
    k = 0.5*(2.*l + 1.) / r # k [Mpc^-1], r [Mpc]. FIXME: Check if h^-1 Mpc or not
    k[np.where(np.isinf(k))] = 1e7 # Remove inf, replace with large number
    
    # Matter power spectrum down past light-cone
    # TODO: Use non-linear matter power instead
    Pk0 = cp.perturbation.power_spectrum(k, 0.0, **cosmo)
    D = cp.perturbation.fgrowth(z, cosmo['omega_M_0'])
    Pk = Pk0 * D**2.
    #Pk[0] = 0.; Pk0[0] = 0. # Get BCs correct
    
    # Calc. obs. CMB dipole and optical depth history (multiply by alpha afterwards)
    #if not use_precomputed:
    #    Beta = np.array([beta(zz, cosmo) for zz in z])
    dtaudr2 = np.abs((1.+z)**4./(1. - ok*(100.*h*r/C)**2.) )
    
    # Perform integration, using either real-space or harmonic-space integral
    if use_harmonic_integral:
        integ = dtaudr2 * Beta**2. * Pk / k**3.
        Cl = scipy.integrate.simps(integ, k)
        Cl *= alpha**2. * 4.*np.pi**2. / (2.*l + 1.)
    else:
        integ = dtaudr2 * Beta**2. * Pk * r
        Cl = scipy.integrate.simps(integ, r)
        Cl *= alpha**2. * 16.*np.pi**2. / (2.*l + 1.)**3.
    
    # Fix k array values and return
    #k[0] = np.nan
    return Cl #, integ, k
    
def ydistortion(cosmo=DEFAULT_COSMO):
    """
    Calculate the Compton y-distortion parameter, defined in Eq. 39 of 
    Zibin & Moss.
    """
    # Redshift range for integration (out to reionisation)
    #z = np.linspace(0., cosmo['z_re'], 50) # FIXME: Accuracy?
    #r = cp.distance.comoving_distance(z, z0=0., **cosmo)
    
    # Scaling constants for optical depth
    fb = cosmo['omega_b_0'] / cosmo['omega_M_0'] # Baryon fraction
    YHe = cosmo['Y_He']; h = cosmo['h']
    om = cosmo['omega_M_0']; ok = cosmo['omega_k_0']
    alpha = 1.15e-5 * fb * om * h**2. * (2. - YHe) # Pre-factor of dTau/dr, in Mpc^-1
    
    # Dipole, and optical depth (multiply by alpha afterwards)
    #Beta = np.array([beta(zz, cosmo) for zz in z])
    dtaudr = (1.+z)**2. / np.sqrt( 1. - ok*(100.*h*r/C)**2. )
    
    # Integrate y-distortion
    integ = dtaudr * Beta**2.
    y = scipy.integrate.simps(integ, r)
    return (7./10.) * alpha * y

def dydr(cosmo=DEFAULT_COSMO):
    """Derivative of the Compton y-distortion parameter."""
    # Redshift range for integration (out to reionisation)
    #z = np.linspace(0., cosmo['z_re'], 50) # FIXME: Accuracy?
    #r = cp.distance.comoving_distance(z, z0=0., **cosmo)
    
    # Scaling constants for optical depth
    fb = cosmo['omega_b_0'] / cosmo['omega_M_0'] # Baryon fraction
    YHe = cosmo['Y_He']; h = cosmo['h']
    om = cosmo['omega_M_0']; ok = cosmo['omega_k_0']
    alpha = 1.15e-5 * fb * om * h**2. * (2. - YHe) # Pre-factor of dTau/dr, in Mpc^-1
    
    # Dipole, and optical depth (multiply by alpha afterwards)
    #Beta = np.array([beta(zz, cosmo) for zz in z])
    dtaudr = (1.+z)**2. / np.sqrt( 1. - ok*(100.*h*r/C)**2. )
    return (7./10.) * alpha * dtaudr * Beta**2.



########################################################################
# Set-up problem and compute Cl's for KSZ
########################################################################

cosmo = DEFAULT_COSMO
ell = np.logspace(0., np.log10(3000.), 50)

# Precompute eta(z)
_z = np.concatenate( ([0.], np.logspace(-5, np.log10(Z_LSS*1.01), 400)) )
_eta = cp.distance.comoving_distance(_z, z0=0., **cosmo)
eta = scipy.interpolate.interp1d(_z, _eta, kind='linear')
z_eta = scipy.interpolate.interp1d(_eta, _z, kind='linear')

# Pre-compute Beta(z) [computationally intensive]
print "Calculating Beta..."
z = np.logspace(-5., np.log10(cosmo['z_re']), 50)
r = cp.distance.comoving_distance(z, z0=0., **cosmo)
Beta = beta(z, cosmo)
print "Finished calculating Beta."

# Calculate Cl's for KSZ
cl = Cl_ksz(ell)

# Plot result
P.subplot(111)
P.plot(ell, cl*ell*(ell+1.)/(2.*np.pi))
P.xlabel("$\ell$")
P.ylabel("$\ell(\ell+1)C_\ell / 2\pi$")
P.yscale('log')
P.xscale('log')
P.show()

#!/usr/bin/python
"""
Plot fractional change in angular diameter distance.
"""

import numpy as np
import cosmolopy as cp
import pylab as P
from matplotlib import ticker
import distortions

cosmo = distortions.DEFAULT_COSMO
Z_LSS = distortions.Z_LSS

# Define parameter space
r0 = np.logspace(3., np.log10(1.2e4), 15)
phi0 = np.logspace(-4., -2., 25)

# Scan through r0 values
y = []
for _r0 in r0:
  m = distortions.Model(phi0=1e0, r0=_r0, cosmo=cosmo)
  _y = distortions.dA_perturbed(Z_LSS, m)
  y.append(_y)
y = np.array(y)
print y

# Construct 2D plot from 1D values
R0, PHI0 = np.meshgrid(r0, phi0)
Y = PHI0 * np.array( [y,]*phi0.size )

P.subplot(111)
#ctr = P.contour(R0, PHI0, Y, colors='k')
#P.clabel(ctr, inline=1, fontsize='x-small', fmt='%1.3f')
ctr = P.contour(R0, PHI0, Y, locator=ticker.LogLocator(), colors='k')
P.clabel(ctr, inline=1, fontsize='small', fmt=ticker.LogFormatterMathtext())
P.xscale('log')
P.yscale('log')
P.xlabel("$r_0 [Mpc]$", fontdict={'fontsize':'20'})
P.ylabel("$\Phi_0$", fontdict={'fontsize':'20'})
P.show()

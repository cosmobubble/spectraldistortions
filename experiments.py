#!/usr/bin/python
"""
Experiment with plotting various quantities.
"""

import numpy as np
import cosmolopy as cp
import pylab as P
import distortions

cosmo = distortions.DEFAULT_COSMO

# Define parameter space
#r0 = np.logspace(2.5, np.log10(1.2e4), 15)
#phi0 = np.logspace(-4.5, -2., 45)

m = distortions.Model(phi0=1e-2, r0=2e3, cosmo=cosmo)

rr = m.r #np.linspace(0., 5e3, 100)
zz = m.z

_beta = []
for _z in zz:
  dzin = distortions.delta_z(m, _z, distortions.Z_LSS, theta=np.pi/2., phi=np.pi)
  dzout = distortions.delta_z(m, _z, distortions.Z_LSS, theta=np.pi/2., phi=0.)
  _beta.append( (1. + _z)*(dzin-dzout) / (2. + 2.*distortions.Z_LSS) )

# Calculate various quantities
beta = distortions.beta(zz, m)
beta_full = [distortions.dipole_full(m, _z) for _z in zz]
vel = distortions.velocity(rr, zz, m)

P.subplot(111)
P.plot(zz, beta, 'r-', lw=1.5)
P.plot(zz, beta_full, 'k-', lw=1.5)
P.plot(zz, _beta, 'y--', lw=1.5)
P.plot(zz, vel/3e5, 'g--', lw=1.5)
P.xlim((0., 4.))
P.ylabel("beta(z)")

P.show()
